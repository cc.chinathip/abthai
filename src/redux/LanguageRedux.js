/**
 * Created by InspireUI on 06/03/2017.
 *
 * @format
 */

import { Config } from "@common";
// import { warn, log } from '@app/Omni'
// import CurrencyWorker from '@services/CurrencyWorker'

const types = {
  CHANGE_LANGUAGE: "th"
};

export const actions = {
  changeLanguage: (dispatch, language) => {
    dispatch({
      type: types.CHANGE_LANGUAGE,
      ...language
    });
  }
};

const initialState = {
  lang: Config.language
};

export const reducer = (state = initialState, action) => {
  const { language } = action;
  switch (action.type) {
    case types.CHANGE_LANGUAGE:
      return Object.assign({}, state, { language });
    default:
      return state;
  }
};
