export default class User {
  constructor(post) {
    const {
      customer_id,
      firstname,
      lastname,
      email,
      telephone,
      address
    } = post

    try {
      this.id = customer_id
      this.first_name = firstname
      this.last_name = lastname
      this.email = email
      this.billing = {
        first_name: firstname,
        last_name: lastname
      }
      this.telephone = telephone,
        this.address = address
      // console.log('telephone', this.telephone)
    } catch (e) {
      console.error(e.message)
    }
  }
}
