import { AppConfig } from "@common";
import axios from "axios";
import {
  Category,
  Product,
  User,
  ShippingMethod,
  PaymentMethod,
  Address,
  MyOrder,
  Review
} from "@mapping";
import moment from "moment";
import { warn } from "@app/Omni";
import _ from "lodash";

export const getInformations = information_id => {
  return new Promise((resolve, reject) => {
    //axios.get(AppConfig.Opencart.url + "/index.php?route=extension/mstore/account")
    axios
      .get(
        `https://abthai.app/index.php?route=extension/mstore/information&information_id=${information_id}`
      )
      .then(({ status, data }) => {
        resolve(status == 200 && data.data ? data.data : null);
      })
      .catch(reject);
  });
};

export const getNotification = () => {
  return new Promise((resolve, reject) => {
    //axios.get(AppConfig.Opencart.url + "/index.php?route=extension/mstore/account")
    axios
      .get("https://abthai.app/index.php?route=extension/mstore/news")
      .then(({ status, data }) => {
        resolve(status == 200 && data.data ? data.data : null);
      })
      .catch(reject);
  });
};

export const ShowLenghProduct = product_id => {
  return new Promise((resolve, reject) => {
    //axios.get(AppConfig.Opencart.url + "/index.php?route=extension/mstore/account")
    axios
      .get(
        `https://abthai.app/index.php?route=extension/mstore/product/ShowLenghProduct&product_id=${product_id}`
      )
      .then(({ status, data }) => {
        resolve(status == 200 && data.data ? data.data : null);
      })
      .catch(reject);
  });
};

export const getUserInfo = () => {
  return new Promise((resolve, reject) => {
    //axios.get(AppConfig.Opencart.url + "/index.php?route=extension/mstore/account")
    axios
      .get("https://abthai.app/index.php?route=extension/mstore/account")
      .then(({ status, data }) => {
        //console.log('data getUserInfo', data)
        resolve(status == 200 && data.data ? data.data : null);
      })
      .catch(reject);
  });
};

export const getBanner = () => {
  return new Promise((resolve, reject) => {
    //axios.get(AppConfig.Opencart.url + "/index.php?route=extension/mstore/account")
    axios
      .get("https://abthai.app/index.php?route=extension/mstore/banner")
      .then(({ status, data }) => {
        console.log("banner api", data);
        resolve(status == 200 && data.data ? data.data : null);
      })
      .catch(reject);
  });
};

export const getCategories = () => {
  return new Promise((resolve, reject) => {
    //axios.get(AppConfig.Opencart.url + "/index.php?route=extension/mstore/category&limit=100")
    axios
      .get("https://abthai.app/index.php?route=extension/mstore/rootcategory")
      .then(({ status, data }) => {
        if (status == 200 && data) {
          resolve(_.map(data.data, item => new Category(item)));
          console.log("Category", item);
        } else {
          resolve([]);
        }
      })
      .catch(reject);
  });
};

export const getAllProducts = (per_page, page, order, orderby) => {
  return new Promise((resolve, reject) => {
    //var url = AppConfig.Opencart.url + `/index.php?route=extension/mstore/product&page=${page}&limit=${per_page}&lang=en`
    var url = `https://abthai.app/index.php?route=extension/mstore/product&page=${page}&limit=${per_page}`;
    axios
      .get(url)
      .then(({ status, data }) => {
        //console.log('data', data)
        if (status == 200 && data.data) {
          resolve(_.map(data.data, item => new Product(item)));
        } else {
          resolve([]);
        }
      })
      .catch(reject);
  });
};

export const productsByCategoryId = (categoryId, per_page, page, filters) => {
  return new Promise((resolve, reject) => {
    //var url = AppConfig.Opencart.url + `/index.php?route=extension/mstore/product&page=${page}&limit=${per_page}&lang=en`
    var url = `https://abthai.app/index.php?route=extension/mstore/product&page=${page}&limit=${per_page}`;
    if (filters.category || categoryId) {
      url += `&category=${filters.category ? filters.category : categoryId}`;
    }
    axios
      .get(url)
      .then(({ status, data }) => {
        if (status == 200 && data.data) {
          resolve(_.map(data.data, item => new Product(item)));
        } else {
          resolve([]);
        }
      })
      .catch(reject);
  });
};

export const productsByCategoryTag = (categoryId, tagId, per_page, page) => {
  return new Promise((resolve, reject) => {
    var url = null;
    if (categoryId) {
      // url = AppConfig.Opencart.url + `/index.php?route=extension/mstore/product&page=${page}&limit=${per_page}&category=${categoryId}&lang=en`
      //url = `https://abthai.app/index.php?route=extension/mstore/product&page=${page}&limit=${per_page}&category=${categoryId}&lang=en`
      url = `https://abthai.app/index.php?route=extension/mstore/subcategory&page=${page}&limit=${per_page}&category=${categoryId}&lang=en`;
    } else {
      //url = AppConfig.Opencart.url + `/index.php?route=extension/mstore/product&page=${page}&limit=${per_page}&tag=${tagId}&lang=en`
      // url = `https://abthai.app/index.php?route=extension/mstore/product&page=${page}&limit=${per_page}&tag=${tagId}&lang=en`
    }
    axios
      .get(url)
      .then(({ status, data }) => {
        //console.log('productsByCategoryTag', data)
        if (status == 200 && data.data) {
          resolve(_.map(data.data, item => new Product(item)));

          console.log("success");
        } else {
          resolve([]);
          console.log("failee");
        }
      })
      .catch(reject);
  });
};

export const productsBySubategory = (subcategoryId, per_page, page) => {
  return new Promise((resolve, reject) => {
    var url = null;
    url = `https://abthai.app/index.php?route=extension/mstore/product&page=${page}&limit=${per_page}&category=${subcategoryId}&lang=en`;

    axios
      .get(url)
      .then(({ status, data }) => {
        //console.log('productsByCategoryTag', data)
        if (status == 200 && data.data) {
          resolve(_.map(data.data, item => new Product(item)));

          console.log("success");
        } else {
          resolve([]);
          console.log("failee");
        }
      })
      .catch(reject);
  });
};

export const productsByName = (name, per_page, page) => {
  return new Promise((resolve, reject) => {
    // var url = AppConfig.Opencart.url + `/index.php?route=extension/mstore/product&page=${page}&limit=${per_page}&search=${name}&lang=en`
    var url = `https://abthai.app/index.php?route=extension/mstore/product&page=${page}&limit=${per_page}&search=${name}&lang=en`;
    /* if (filters.category) {
            url += `&category=${filters.category}`
        }
        if (filters.max_price) {
            url += `&max_price=${filters.max_price}`
        } */
    axios
      .get(url)
      .then(({ status, data }) => {
        if (status == 200 && data.data) {
          resolve(_.map(data.data, item => new Product(item)));
        } else {
          resolve([]);
        }
      })
      .catch(reject);
  });
};

export const register = ({ email, firstName, lastName, password, address }) => {
  return new Promise((resolve, reject) => {
    //axios.post(AppConfig.Opencart.url + "/index.php?route=extension/mstore/account/register", {
    axios
      .post(
        "https://abthai.app/index.php?route=extension/mstore/account/register",
        {
          firstname: firstName,
          lastname: lastName,
          email,
          agree: "1",
          password,
          confirm: password,

          address
        }
      )
      .then(({ status, data }) => {
        logout();
        resolve(new User(data.data));
      })
      .catch(err => {
        const message =
          err.response && err.response.data.error
            ? err.response.data.error[0]
            : "Server don't response correctly";
        reject(message);
      });
  });
};

export const getUserByEmail = email => {
  return new Promise((resolve, reject) => {
    //axios.get(AppConfig.Opencart.url + "/index.php?route=extension/mstore/account")
    axios
      .post(
        "https://abthai.app/index.php?route=extension/mstore/account/getUserByEmail",
        email
      )
      .then(res => {
        console.log("data getUserInfo", res);
        resolve({ user: new User(res.data.data), token: "No token" });
      })
      .catch(reject);
  });
};

export const login = (username, password) => {
  return new Promise((resolve, reject) => {
    logout();
    //axios.post(AppConfig.Opencart.url + "/index.php?route=extension/mstore/account/login", {
    axios
      .post(
        "https://abthai.app/index.php?route=extension/mstore/account/login",
        {
          email: username,
          password
        }
      )
      .then(res => {
        resolve({ user: new User(res.data.data), token: "No token" });
      })
      .catch(err => {
        const message =
          err.response && err.response.data.error
            ? err.response.data.error[0]
            : "Server don't response correctly";
        reject(message);
      });
  });
};

export const logout = () => {
  //axios.get(AppConfig.Opencart.url + "/index.php?route=extension/mstore/account")
  axios.get("https://abthai.app/index.php?route=extension/mstore/account");
};

export const emptyCart = () => {
  return new Promise((resolve, reject) => {
    //axios.delete(AppConfig.Opencart.url + "/index.php?route=extension/mstore/cart/empty")
    axios
      .delete("https://abthai.app/index.php?route=extension/mstore/cart/empty")
      .then(({ status, data }) => {
        resolve(data);
      })
      .catch(err => {
        const message =
          err.response && err.response.data.error
            ? err.response.data.error[0]
            : "Server don't response correctly";
        reject(message);
      });
  });
};

export const addItemsToCart = (carts, value, option_array) => {
  return new Promise((resolve, reject) => {
    console.log("addItemsToCart carts2", option_array);
    const params = _.map(carts, item => ({
      product_id: item,
      quantity: "1",
      option: option_array
    }));
    //axios.post(AppConfig.Opencart.url + "/index.php?route=extension/mstore/cart/add", params)
    axios
      .post(
        "https://abthai.app/index.php?route=extension/mstore/cart/add",
        params
      )
      .then(({ status, data }) => {
        //console.log('addItemsToCart data', data)
        resolve(data);
      })
      .catch(err => {
        //console.log('addItemsToCart err', err)
        const message =
          err.response && err.response.data.error
            ? err.response.data.error[0]
            : "Server don't response correctly";
        reject(message);
      });
  });
};

export const addItemsToCart2 = (product_id, quantity, options) => {
  return new Promise((resolve, reject) => {
    const params = { product_id: product_id, quantity: "1", option: options };
    //axios.post(AppConfig.Opencart.url + "/index.php?route=extension/mstore/cart/add", params)
    axios
      .post(
        "https://abthai.app/index.php?route=extension/mstore/cart/add",
        params
      )
      .then(({ status, data }) => {
        //console.log('addItemsToCart data', data)
        resolve(data);
      })
      .catch(err => {
        //console.log('addItemsToCart err', err)
        const message =
          err.response && err.response.data.error
            ? err.response.data.error[0]
            : "Server don't response correctly";
        reject(message);
      });
  });
};

export const setShippingAddress = address => {
  return new Promise((resolve, reject) => {
    //axios.post(AppConfig.Opencart.url + "/index.php?route=extension/mstore/shipping_address/save", address)
    axios
      .post(
        "https://abthai.app/index.php?route=extension/mstore/shipping_address/save",
        address
      )
      .then(({ status, data }) => {
        resolve(data);
      })
      .catch(err => {
        const message =
          err.response && err.response.data.error
            ? err.response.data.error[0]
            : "Server don't response correctly";
        reject(message);
      });
  });
};

export const getShippingMethod = (address, token) => {
  return new Promise((resolve, reject) => {
    setShippingAddress(new Address(address))
      .then(() => {
        //axios.get(AppConfig.Opencart.url + "/index.php?route=extension/mstore/shipping_method")
        axios
          .get(
            "https://abthai.app/index.php?route=extension/mstore/shipping_method"
          )

          .then(({ status, data }) => {
            var items = [];
            Object.values(data.data.shipping_methods).forEach(item => {
              var shippingMethod = new ShippingMethod(item);
              items.push(shippingMethod);
            });
            resolve(items);
          })
          .catch(err => {
            const message =
              err.response && err.response.data.error
                ? err.response.data.error[0]
                : "Server don't response correctly";
            reject(message);
          });
      })
      .catch(reject);
  });
};

export const setPaymentAddress = address => {
  return new Promise((resolve, reject) => {
    //axios.post(AppConfig.Opencart.url + "/index.php?route=extension/mstore/payment_address/save", address)
    axios
      .post(
        "https://abthai.app/index.php?route=extension/mstore/payment_address/save",
        address
      )

      .then(({ status, data }) => {
        resolve(data);
      })
      .catch(err => {
        const message =
          err.response && err.response.data.error
            ? err.response.data.error[0]
            : "Server don't response correctly";
        reject(message);
      });
  });
};

export const getPayments = (address, token) => {
  return new Promise((resolve, reject) => {
    setPaymentAddress(new Address(address))
      .then(() => {
        //axios.get(AppConfig.Opencart.url + "/index.php?route=extension/mstore/payment_method")
        axios
          .get(
            "https://abthai.app/index.php?route=extension/mstore/payment_method"
          )

          .then(({ status, data }) => {
            var items = [];
            Object.values(data.data.payment_methods).forEach(item => {
              var paymentMethod = new PaymentMethod(item);
              items.push(paymentMethod);
            });
            resolve(items);
          })
          .catch(err => {
            const message =
              err.response && err.response.data.error
                ? err.response.data.error[0]
                : "Server don't response correctly";
            reject(message);
          });
      })
      .catch(reject);
  });
};

export const setShippingMethod = shipping_method => {
  return new Promise((resolve, reject) => {
    //axios.post(AppConfig.Opencart.url + "/index.php?route=extension/mstore/shipping_method/save", { shipping_method, comment: "string" })
    axios
      .post(
        "https://abthai.app/index.php?route=extension/mstore/shipping_method/save",
        { shipping_method, comment: "string" }
      )

      .then(({ status, data }) => {
        resolve(data);
      })
      .catch(err => {
        const message =
          err.response && err.response.data.error
            ? err.response.data.error[0]
            : "Server don't response correctly";
        reject(message);
      });
  });
};

export const setPaymentMethod = payment_method => {
  return new Promise((resolve, reject) => {
    //axios.post(AppConfig.Opencart.url + "/index.php?route=extension/mstore/payment_method/save", { payment_method, comment: "string", "agree": "1" })
    axios
      .post(
        "https://abthai.app/index.php?route=extension/mstore/payment_method/save",
        { payment_method, comment: "string", agree: "1" }
      )

      .then(({ status, data }) => {
        resolve(data);
      })
      .catch(err => {
        const message =
          err.response && err.response.data.error
            ? err.response.data.error[0]
            : "Server don't response correctly";
        reject(message);
      });
  });
};

export const createNewOrder = async (payload = "", onFinish, onError) => {
  try {
    //await axios.get("https://abthai.app/index.php?route=extension/mstore/shipping_method")
    //await setShippingMethod(payload.shipping_method)

    //await axios.get("https://abthai.app/index.php?route=extension/mstore/payment_method")
    //await setPaymentMethod(payload.payment_method)

    var payment_address = payload.payment_address;

    await axios.post(
      "https://abthai.app/index.php?route=extension/mstore/order/confirm",
      payment_address
    );

    onFinish();
  } catch (error) {
    onError(error);
  }
};

export const ordersByCustomerId = (customer_id, per_page, page) => {
  return new Promise((resolve, reject) => {
    //axios.get(AppConfig.Opencart.url + `/index.php?route=extension/mstore/order/orders&limit=${per_page}&page=${page}`)
    axios
      .get(
        `https://abthai.app/index.php?route=extension/mstore/order/orders&limit=${per_page}&page=${page}`
      )
      .then(({ status, data }) => {
        console.log("ordersByCustomerId", data);
        if (status == 200 && data.data) {
          var items = [];
          data.data.forEach(item => {
            items.push(new MyOrder(item));
          });
          resolve(items);
        } else {
          resolve([]);
        }
      })
      .catch(reject);
  });
};

// call data review
export const reviewsByProductId = product_id => {
  return new Promise((resolve, reject) => {
    //axios.get(AppConfig.Opencart.url + `/index.php?route=extension/mstore/review&id=${product_id}`)
    axios
      .get(
        `https://abthai.app/index.php?route=extension/mstore/review&id=${product_id}`
      )
      .then(({ status, data }) => {
        if (status == 200 && data.data) {
          var items = [];
          data.data.forEach(item => {
            items.push(new Review(item));
          });
          resolve(items);
        } else {
          resolve([]);
        }
      })
      .catch(reject);
  });
};

// send review
export const postReview = (product_id, name, text, rating) => {
  return new Promise((resolve, reject) => {
    //axios.post(AppConfig.Opencart.url + `/index.php?route=extension/mstore/review&id=${product_id}`, { name, text, rating })
    axios
      .post(
        `https://abthai.app/index.php?route=extension/mstore/review&id=${product_id}`,
        { name, text, rating }
      )
      .then(resolve)
      .catch(err => {
        const message =
          err.response && err.response.data.error
            ? err.response.data.error[0]
            : "Server don't response correctly";
        reject(message);
      });
  });
};

export const postRequire = (product_id, email, width, length) => {
  return new Promise((resolve, reject) => {
    //axios.post(AppConfig.Opencart.url + `/index.php?route=extension/mstore/review&id=${product_id}`, { name, text, rating })
    axios
      .post(
        `https://abthai.app/index.php?route=extension/mstore/productrequire&id=${product_id}`,
        { email, width, length }
      )
      .then(resolve)
      .catch(err => {
        const message =
          err.response && err.response.data.error
            ? err.response.data.error[0]
            : "Server don't response correctly";
        reject(message);
      });
  });
};

export const postRequire2 = (carts, email) => {
  return new Promise((resolve, reject) => {
    const params = _.map(carts, item => ({
      product_id: item.product.id,
      quantity: item.quantity,
      width: item.width,
      length: item.length,
      email: email
    }));
    //axios.post(AppConfig.Opencart.url + "/index.php?route=extension/mstore/cart/add", params)
    axios
      .post(
        "https://abthai.app/index.php?route=extension/mstore/cart/add2",
        params
      )
      .then(({ status, data }) => {
        resolve(data);
      })
      .catch(err => {
        const message =
          err.response && err.response.data.error
            ? err.response.data.error[0]
            : "Server don't response correctly";
        reject(message);
      });
  });
};

export const addNewOrder = async (payload = "", onFinish, onError) => {
  try {
    //console.log("addNewOrder", payload.address, "taxid", payload.taxid);
    const shipping_carts = _.map(payload.carts, item => ({
      product_id: item.product.product_id,
      quantity: item.quantity,
      width: item.width,
      length: item.length,
      priceItems: item.priceItems,
      option_id: item.option_id,
      width_option_id: item.width_option_id,
      address: payload.address,
      taxid: payload.taxid
    }));

    //await axios.get("https://abthai.app/index.php?route=extension/mstore/shipping_method")
    //await setShippingMethod(payload.shipping_method)

    //await axios.get("https://abthai.app/index.php?route=extension/mstore/payment_method")
    //await setPaymentMethod(payload.payment_method)

    //var payment_address = payload.payment_address;

    await axios.post(
      "https://abthai.app/index.php?route=extension/mstore/order/orderconfirm",
      shipping_carts
    );

    onFinish();
  } catch (error) {
    onError(error);
  }
};

export const ShowCategoryProduct = product_id => {
  return new Promise((resolve, reject) => {
    //axios.get(AppConfig.Opencart.url + "/index.php?route=extension/mstore/account")
    axios
      .get(
        `https://abthai.app/index.php?route=extension/mstore/product/ShowCategoryProduct&product_id=${product_id}`
      )
      .then(({ status, data }) => {
        resolve(status == 200 && data.data ? data.data : null);
      })
      .catch(reject);
  });
};
