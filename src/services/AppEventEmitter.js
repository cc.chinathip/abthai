/** @format */

import { NativeEventEmitter } from "react-native";

// import EventEmitter from "EventEmitter";

var AppEventEmitter = new NativeEventEmitter();

export default AppEventEmitter;
