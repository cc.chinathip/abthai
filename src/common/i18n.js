
import I18n from 'react-native-i18n';

I18n.fallbacks = true;
I18n.translations = {
    'en': require('./lang/en.json'),
    'th': require('./lang/th.json')
};

export default I18n;