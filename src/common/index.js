/** @format */

import { withTheme as _withTheme } from "@callstack/react-theme-provider";
import { withNavigation as _withNavigation } from "react-navigation";
import * as OpencartWorker from "../services/OpencartWorker";
import _Color from "./Color";
import _Constants from "./Constants";
import _Config from "./Config";
import _AppConfig from "./AppConfig.json";
import _Icons from "./Icons";
import _Images from "./Images";
import _Languages from "./Languages";
import _Languages_th from "./Languages_th";
import _Styles from "./Styles";
import _Tools from "./Tools";
import _Layout from "./Layout";
import _Validator from "./Validator";
import _Events from "./Events";
import _Device from "./Device";
import _Theme from "./Theme";
/* import i18n from "./i18n"; */

export const Color = _Color;
export const Constants = _Constants;
export const Config = _Config;
export const AppConfig = _AppConfig;
export const Icons = _Icons;
export const Images = _Images;
export const Languages = _Languages;
export const Languages_th = _Languages_th;
export const Styles = _Styles;
export const Tools = _Tools;
export const Layout = _Layout;




//export const lang18 = i18n;



/* import I18n from 'react-native-i18n'
I18n.fallbacks = true;
I18n.translations = {
  _Languages,
  _Languages_th
};


export const I18n = I18n; */

console.log('language', Languages);


var categoryall_array = OpencartWorker.getCategories()
  .then(items => { })
  .catch(err => {
    //console.log('OpencartWorker.getCategories()', err)
  });

//console.log('categoryall222', OpencartWorker.getCategories())

export const HorizonLayouts = [OpencartWorker.getCategories()];
export const Validator = _Validator;
export const Events = _Events;
export const Device = _Device;
export const Theme = _Theme;
export const withTheme = _withTheme;
export const withNavigation = _withNavigation;
