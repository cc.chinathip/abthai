/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import {
  Text,
  TextInput,
  TouchableOpacity,
  FlatList,
  View,
  Animated,
  Image,
  Share,
  Alert
} from "react-native";
import { Picker } from "native-base";
import { connect } from "react-redux";
import { Timer, getProductImage, log } from "@app/Omni";
import {
  Button,
  AdMob,
  WebView,
  ProductSize as ProductAttribute,
  ProductColor,
  ProductRelated,
  Rating
} from "@components";
import Swiper from "react-native-swiper";
import {
  Styles,
  Languages,
  Color,
  Config,
  Constants,
  Events,
  withTheme,
  Tools
} from "@common";
import Modal from "react-native-modalbox";
import { find, filter, findIndex } from "lodash";
import * as Animatable from "react-native-animatable";
import AttributesView from "./AttributesView";
import ReviewTab from "./ReviewTab.js";
import styles from "./ProductDetail_Style";
import PSelect from "react-native-select-plus";
import css from "./cssstyles";
import Icon from "@expo/vector-icons/SimpleLineIcons";
import * as OpencartWorker from "@services/OpencartWorker";

import { toast } from "@app/Omni";

const PRODUCT_IMAGE_HEIGHT = 300;
const NAVI_HEIGHT = 64;

class Detail extends PureComponent {
  static propTypes = {
    product: PropTypes.any,
    getProductVariations: PropTypes.func,
    productVariations: PropTypes.any,
    onViewCart: PropTypes.func,
    addCartItem: PropTypes.func,
    removeWishListItem: PropTypes.func,
    addWishListItem: PropTypes.func,
    cartItems: PropTypes.any,
    navigation: PropTypes.object,
    user: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(0),
      tabIndex: 0,
      emailsend: "",
      productsend: "",
      widthmm: "",
      // selectedAttribute: [],
      // selectedColor: 0,
      selectVariation: null,
      selectedImageId: 0, // is placeholder image
      selectedImage: null,
      value: "เลือกความยาว...",
      length: [],
      enabled: false,
      price_item: 0
      //isLoading: false,
    };

    this.productInfoHeight = PRODUCT_IMAGE_HEIGHT;
    this.inCartTotal = 0;
    this.isInWishList = false;
    /*     this.items = [
              { key: 1, section: true },
              { key: 2, label: "54 XL  137.16 x 380 mm." },
              { key: 3, label: "60 XL 152.40 x 380 mm." },
              { key: 4, label: "70 XL 177.80 x 380 mm." },
              { key: 5, label: "80 XL 380 x 203.20 mm." },
              { key: 6, label: "90 XL 380 x 228.60 mm." },
              { key: 7, label: "100 XL 380 x 254.00 mm." }
            ]; */
  }
  componentWillMount() {
    console.log("this.props.product", this.props.product);

    /* console.log('this.props.product.options', this.state.parent_category_id) */

    this.props.product.options.map((value, index) => {
      if (index == 0) {
        //console.log('this.props.product.options',' ',value)

        this.setState({
          checklength: value.product_option_value.length
        });
      }
    });

    OpencartWorker.ShowLenghProduct(this.props.product.product_id)
      .then(res => {
        //login(res.user, res.token);
        //console.log('LenghProduct data', res.name)
        this.setState({
          length: res
        });
      })
      .catch(message => {
        console.log("LenghProduct data", message);
      });

    OpencartWorker.ShowCategoryProduct(this.props.product.product_id)
      .then(res => {
        //login(res.user, res.token);
        console.log("ShowCategoryProduct data", res);

        res.map((value, index) => {
          console.log("parent_category", value.name);

          this.setState({
            parent_category: value.name
          });
        });
      })
      .catch(message => {
        console.log("ShowCategoryProduct data", message);
      });

    /*  OpencartWorker.getUserByEmail('cc.chinathip@gmail.com')
           .then((res) => {
             //login(res.user, res.token);
             console.log('user data', res)
           })
           .catch((message) => {
             console.log('user data', message)
           }) */
  }

  componentDidMount() {
    this.getCartTotal(this.props);
    this.getWishList(this.props);

    this.getProductAttribute(this.props.product);
    this.props.getProductVariations(this.props.product);
  }

  componentWillReceiveProps(nextProps) {
    this.getCartTotal(nextProps, true);
    this.getWishList(nextProps, true);

    // this important to update the variations from the product as the Life cycle is not run again !!!

    if (this.props.product.product_id != nextProps.product.product_id) {
      this.props.getProductVariations(nextProps.product);
      this.getProductAttribute(nextProps.product);
      this.forceUpdate();
    }

    if (this.props.productVariations !== nextProps.productVariations) {
      this.updateSelectedVariant(nextProps.productVariations);
    }
  }

  getProductAttribute = product => {
    this.productAttributes = product.attributes;
    const defaultAttribute = product.default_attributes;

    if (typeof this.productAttributes !== "undefined") {
      this.productAttributes.map(attribute => {
        const selectedAttribute = defaultAttribute.find(
          item => item.name === attribute.name
        );
        attribute.selectedOption =
          typeof selectedAttribute !== "undefined"
            ? selectedAttribute.option.toLowerCase()
            : "";
      });
    }
  };

  closePhoto = () => {
    this._modalPhoto.close();
  };

  openPhoto = () => {
    this._modalPhoto.open();
  };

  closeTest = () => {
    this._modalTest.close();
  };

  openTest = () => {
    this._modalTest.open();
  };

  handleClickTab(tabIndex) {
    this.setState({ tabIndex });
    Timer.setTimeout(() => this.state.scrollY.setValue(0), 50);
  }

  onSelectedItemsChange = value => {
    this.setState({
      value: value.name,
      enabled: true,
      price_item: value.price,
      length_option: value.product_option_value_id
      //option_id: value.product_option_value_id,
    });

    console.log("onSelectedItemsChange", value);
  };

  renderPicker() {
    console.log("checklength:0", this.state.checklength);

    if (this.state.checklength > 0) {
      return (
        <Picker
          iosIcon={<Icon name="arrow-down" />}
          placeholder={this.state.value}
          placeholderStyle={{ color: "black" }}
          iosHeader="โปรดเลือก..."
          enabled={this.state.enable}
          mode="dropdown"
          style={{ width: 250 }}
          selectedValue={this.state.value}
          onValueChange={this.onSelectedItemsChange.bind(this)}
        >
          {this.props.product.options.map((value, index) => {
            //console.log('this.state.value', this.state.value)

            if (value.type === "text") {
              this.setState({
                width_option_id: value.product_option_id
              });
            }

            return value.product_option_value.map((value2, index) => {
              return (
                <Picker.Item label={value2.name} value={value2} key={index} />
              );
            });
          })}
        </Picker>
      );
    } else {
      return (
        <TextInput
          style={{
            margin: 5,
            height: 30,
            borderColor: "#000000",
            borderWidth: 1
          }}
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
          onChangeText={text => this.setState({ value: text })}
        />
      );
    }
  }

  getColor = value => {
    const color = value.toLowerCase();
    if (typeof Color.attributes[color] !== "undefined") {
      return Color.attributes[color];
    }
    return "#333";
  };

  share = () => {
    Share.share({
      message: this.props.product.description.replace(/(<([^>]+)>)/gi, ""),
      url: this.props.product.permalink,
      title: this.props.product.name
    });
  };

  addToCart = (go = false, option_value) => {
    const { addCartItem, product, onViewCart } = this.props;

    if (this.state.value === "" || this.state.value === null) {
      Alert.alert("Please Input Product Length !");
      return false;
    }

    if (this.state.parent_category !== "V-BELTS") {
      if (this.state.widthmm === "") {
        Alert.alert("Please Input Product Width !");
        return false;
      }
    }

    //this._modalTest.open();

    //console.log('this.state.value', this.state.value)

    if (this.inCartTotal < Constants.LimitAddToCart) {
      //console.log('addToCart', this.state.selectVariation, this.state.widthmm, this.state.value, this.state.price_item)
      // this.state.price_item คือ ราคาของสินค้า

      //if (this.props.product.options.length > 0) {
      if (this.state.checklength > 0) {
        this.props.product.options.map((value, index) => {
          if (value.type === "select") {
            option_array = {
              [value.product_option_id]: this.state.length_option,
              [this.state.width_option_id]: this.state.widthmm
            };
          }
        });
      } else {
        this.props.product.options.map((value, index) => {
          option_array = {
            [value.product_option_id]: this.state.value,
            [this.state.width_option_id]: this.state.widthmm
          };
        });
      }

      //console.log('option_array', this.props.product.product_id)

      OpencartWorker.addItemsToCart(this.props.product, "1", option_array)

        .then(res => {
          console.log("addItemsToCartaddItemsToCart", res);

          this.setState({
            length: res
          });
        })
        .catch(message => {
          console.log("LenghProduct data", message);
        });

      addCartItem(
        product,
        this.state.selectVariation,
        this.state.widthmm,
        this.state.value,
        this.state.FinalTotal,
        option_array
      );
    } else {
      alert(Languages.ProductLimitWaring);
    }
    if (go) onViewCart();
  };

  addToBuyNow = (go = false) => {
    const { addCartItem, product, onViewCart } = this.props;

    if (this.state.value === "" || this.state.value === null) {
      Alert.alert("Please Input Product Length !");
      return false;
    }
    if (this.state.parent_category !== "V-BELTS") {
      if (this.state.widthmm === "") {
        Alert.alert("Please Input Product Width !");
        return false;
      }
    }

    //this._modalTest.open();

    if (this.inCartTotal < Constants.LimitAddToCart) {
      addCartItem(
        product,
        this.state.selectVariation,
        this.state.widthmm,
        this.state.value,
        this.state.FinalTotal,
        this.state.option_id
      );
    } else {
      alert(Languages.ProductLimitWaring);
    }
    if (go) onViewCart();
  };

  addToWishList = isAddWishList => {
    if (isAddWishList) {
      this.props.removeWishListItem(this.props.product);
    } else this.props.addWishListItem(this.props.product);
  };

  // submit orders for buynow
  submitComment = () => {
    OpencartWorker.postRequire(
      this.props.product.product_id,
      this.state.emailsend,
      this.state.widthmm,
      this.state.value
    )
      .then(() => {
        this.setState({
          emailsend: ""
        });
        toast("Send Data Successful");
        this._modalTest.close();
      })
      .catch(err => {
        console.log(
          "this.props.product.id =",
          this.props.product.product_id,
          "this.state.emailsend =",
          this.state.emailsend,
          "this.state.widthmm=",
          this.state.widthmm,
          "this.state.value=",
          this.state.value
        );
        alert("Send Data Failed");
      });

    /* OpencartWorker.createNewOrder(this.props.product.product_id, this.state.emailsend, this.state.widthmm, this.state.value).then(() => {
          this.setState({
            emailsend: "",
          });
          toast("Send Data Successful");
          this._modalTest.close();
        })
          .catch((err) => {
            //console.log('this.props.product.id =', this.props.product.product_id, 'this.state.emailsend =', this.state.emailsend, 'this.state.widthmm=', this.state.widthmm, 'this.state.value=', this.state.value)
            alert('Send Data Failed')
          }) */
  };

  getCartTotal = (props, check = false) => {
    const { cartItems } = props;

    if (cartItems != null) {
      if (check === true && props.cartItems === this.props.cartItems) {
        return;
      }

      this.inCartTotal = cartItems.reduce((accumulator, currentValue) => {
        if (currentValue.product.product_id == this.props.product.product_id) {
          return accumulator + currentValue.quantity;
        }
        return 0;
      }, 0);

      const sum = cartItems.reduce(
        (accumulator, currentValue) => accumulator + currentValue.quantity,
        0
      );
      const params = this.props.navigation.state.params;
      params.cartTotal = sum;
      this.props.navigation.setParams(params);
    }
  };

  getWishList = (props, check = false) => {
    const { product, navigation, wishListItems } = props;

    if (props.hasOwnProperty("wishListItems")) {
      if (check == true && props.wishListItems == this.props.wishListItems) {
        return;
      }
      this.isInWishList =
        find(props.wishListItems, item => item.product.id == product.id) !=
        "undefined";

      const sum = wishListItems.length;
      const params = navigation.state.params;
      params.wistListTotal = sum;
      this.props.navigation.setParams(params);
    }
  };

  onSelectAttribute = (attributeName, option) => {
    const selectedAttribute = this.productAttributes.find(
      item => item.name === attributeName
    );
    selectedAttribute.selectedOption = option.toLowerCase();

    this.updateSelectedVariant(this.props.productVariations);
  };

  updateSelectedVariant = productVariations => {
    let hasAttribute = false;
    const defaultVariant =
      productVariations && productVariations.length
        ? productVariations[0]
        : null;
    // filter selectedOption null or don't have variation
    const selectedAttribute = filter(
      this.productAttributes,
      item =>
        (item.selectedOption && item.selectedOption !== "") || item.variation
    );
    let selectedImage =
      (defaultVariant && defaultVariant.image && defaultVariant.image.src) ||
      "";
    let selectedImageId = 0;

    if (productVariations && productVariations.length) {
      productVariations.map(variant => {
        let matchCount = 0;
        selectedAttribute.map(selectAttribute => {
          const isMatch = find(
            variant.attributes,
            item =>
              item.name.toUpperCase() === selectAttribute.name.toUpperCase() &&
              item.option.toUpperCase() ===
                selectAttribute.selectedOption.toUpperCase()
          );
          if (isMatch !== undefined) {
            matchCount += 1;
          }
        });

        if (matchCount === selectedAttribute.length) {
          hasAttribute = true;
          selectedImage = (variant.image && variant.image.src) || "";
          selectedImageId = variant.image.id;
          this.setState({
            selectVariation: variant,
            selectedImage,
            selectedImageId
          });
        }
      });
    }

    // set default variant
    if (!hasAttribute && defaultVariant) {
      this.setState({
        selectVariation: defaultVariant,
        selectedImage,
        selectedImageId
      });
    }

    this.forceUpdate();
  };

  /**
   * render Image top
   */
  _renderImages = () => {
    const { selectedImage, selectedImageId } = this.state;
    const imageScale = this.state.scrollY.interpolate({
      inputRange: [-300, 0, NAVI_HEIGHT, this.productInfoHeight / 2],
      outputRange: [2, 1, 1, 0.7],
      extrapolate: "clamp"
    });

    // set variant image and only show when do not placeholder image
    if (selectedImage && selectedImageId !== 0)
      return (
        <View
          style={{
            // height: PRODUCT_IMAGE_HEIGHT,
            width: Constants.Window.width
          }}
        >
          <TouchableOpacity activeOpacity={1} onPress={this.openPhoto}>
            <Animated.Image
              source={{
                uri: getProductImage(this.state.selectedImage, Styles.width)
              }}
              style={[
                styles.imageProduct,
                { transform: [{ scale: imageScale }] }
              ]}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
      );
    const { user } = this.props;

    return (
      <FlatList
        contentContainerStyle={{ paddingLeft: Styles.spaceLayout }}
        ref={comp => (this._photos = comp)}
        data={this.props.product.image}
        renderItem={({ item, index }) => {
          //console.log('666666666666666', this.props.product.image)
          return (
            <TouchableOpacity
              activeOpacity={1}
              key={index}
              onPress={this.openPhoto}
            >
              <Animated.Image
                source={{
                  uri: getProductImage(this.props.product.image, Styles.width)
                }}
                style={[
                  styles.imageProduct,
                  { transform: [{ scale: imageScale }] }
                ]}
                resizeMode="contain"
              />
            </TouchableOpacity>
          );
        }}
        keyExtractor={(item, index) => item.id || index.toString()}
        showsHorizontalScrollIndicator={false}
        horizontal
        pagingEnabled
      />
    );
  };

  /**
   * Render tabview detail
   */
  _renderTabView = () => {
    const {
      theme: {
        colors: { background, text, lineColor }
      }
    } = this.props;

    return (
      <View style={[styles.tabView, { backgroundColor: background }]}>
        <View
          style={[
            styles.tabButton,
            { backgroundColor: lineColor },
            { borderTopColor: lineColor },
            { borderBottomColor: lineColor },
            Constants.RTL && { flexDirection: "row-reverse" }
          ]}
        >
          <View style={[styles.tabItem, { backgroundColor: lineColor }]}>
            <Button
              type="tab"
              textStyle={[styles.textTab, { color: text }]}
              selectedStyle={{ color: text }}
              text={Languages.AdditionalInformation}
              onPress={() => this.handleClickTab(0)}
              selected={this.state.tabIndex == 0}
            />
          </View>
          <View style={[styles.tabItem, { backgroundColor: lineColor }]}>
            <Button
              type="tab"
              textStyle={[styles.textTab, { color: text }]}
              selectedStyle={{ color: text }}
              text={Languages.ProductReviews}
              onPress={() => this.handleClickTab(2)}
              selected={this.state.tabIndex == 2}
            />
          </View>
        </View>
        {this.state.tabIndex === 0 && (
          <View style={[styles.description, { backgroundColor: lineColor }]}>
            <WebView
              textColor={text}
              html={`${this.props.product.description}`}
            />
          </View>
        )}
        {this.state.tabIndex === 2 && (
          <ReviewTab product={this.props.product} />
        )}
      </View>
    );
  };

  _writeReview = () => {
    const { product, userData, onLogin } = this.props;
    if (userData) {
      //console.log('Review', userData)
      Events.openModalReview(product);
      //this._renderTabView()
      console.log("click button  review");
    } else {
      onLogin();
    }
  };

  render() {
    const { selectVariation } = this.state;
    const {
      wishListItems,
      onViewProductScreen,
      product,
      cartItems,
      theme: {
        colors: { background, text, lineColor },
        dark
      }
    } = this.props;

    const isAddToCart = !!(
      cartItems &&
      cartItems.filter(item => item.product.product_id === product.product_id)
        .length > 0
    );
    const isAddWishList =
      wishListItems.filter(
        item => item.product.product_id === product.product_id
      ).length > 0;
    const productPriceDetail = Tools.getPriceIncluedTaxAmount(
      product,
      selectVariation,
      "yes"
      /* this.state.price_item */
    );

    const productRegularPrice = Tools.getCurrecyFormatted(
      selectVariation ? selectVariation.regular_price : product.regular_price
    );
    const isOnSale = selectVariation
      ? selectVariation.on_sale
      : product.on_sale;

    const renderButtons = () => (
      <View
        style={[
          styles.bottomView,
          dark && { borderTopColor: lineColor },
          Constants.RTL && { flexDirection: "row-reverse" }
        ]}
      >
        <View
          style={[
            styles.buttonContainer,
            dark && { backgroundColor: lineColor }
          ]}
        >
          <Button
            type="image"
            source={require("@images/icons/icon-share.png")}
            imageStyle={[styles.imageButton, { tintColor: text }]}
            buttonStyle={styles.buttonStyle}
            onPress={this.share}
          />
          <Button
            type="image"
            isAddWishList={isAddWishList}
            source={require("@images/icons/icon-love.png")}
            imageStyle={[styles.imageButton, { tintColor: text }]}
            buttonStyle={styles.buttonStyle}
            onPress={() => this.addToWishList(isAddWishList)}
          />
          {!Config.HideCartAndCheckout && (
            <Button
              type="image"
              isAddToCart={isAddToCart}
              source={require("@images/icons/icon-cart.png")}
              imageStyle={[styles.imageButton, { tintColor: text }]}
              disabled={!product.in_stock}
              buttonStyle={styles.buttonStyle}
              onPress={() =>
                product.in_stock &&
                this.addToCart(true, this.state.option_array)
              }
            />
          )}
        </View>
        {!Config.HideCartAndCheckout && (
          <Button
            text={product.in_stock ? Languages.BUYNOW : Languages.OutOfStock}
            style={[styles.btnBuy, !product.in_stock && styles.outOfStock]}
            textStyle={styles.btnBuyText}
            disabled={!product.in_stock}
            onPress={() => {
              product.in_stock && this.addToBuyNow(true);
            }}
          />
        )}
      </View>
    );

    const renderRating = () => {
      return (
        <View style={styles.price_wrapper(background)}>
          <Rating rating={Number(product.average_rating)} size={19} />

          <Text style={[styles.textRating, { color: text }]}>
            {`(${product.rating_count})`}
          </Text>

          <TouchableOpacity onPress={this._writeReview}>
            <Text style={[styles.textRating, { color: text }]}>
              {Languages.writeReview}
            </Text>
          </TouchableOpacity>
        </View>
      );
    };

    const { value } = this.state;

    // ราคารวมทั้งหมด
    this.setState({
      /* FinalTotal: parseFloat(this.state.price_item) + parseFloat(productPriceDetail), */
      FinalTotal:
        parseFloat(this.state.price_item) + parseFloat(productPriceDetail)
    });

    const renderWidth = () => {
      if (this.state.parent_category === "V-BELTS") {
        console.log("yes V-Belts");
      } else {
        console.log("no V-Belts");

        console.log("acc", this.state.parent_category);

        if (this.state.parent_category !== "ACCU-LINK") {
          return (
            <View>
              <Text style={[styles.productName, { color: text }]}>
                Width(mm.) *
              </Text>
              <TextInput
                style={[styles.input_width]}
                placeholderTextColor="#9a73ef"
                autoCapitalize="none"
                onChangeText={text => this.setState({ widthmm: text })}
              />
            </View>
          );
        }
      }
    };

    const renderTitle = () => (
      <View style={[styles.title_]}>
        <Text style={[styles.productName, { color: text }]}>
          {product.name}
        </Text>
        <View style={[styles.view_price]}>
          <Animatable.Text
            animation="fadeInDown"
            style={[styles.productPrice, { color: text }]}
          >
            {parseFloat(this.state.price_item) + parseFloat(productPriceDetail)}
          </Animatable.Text>
          {isOnSale && (
            <Animatable.Text
              animation="fadeInDown"
              style={[styles.sale_price, { color: text }]}
            >
              {productRegularPrice}
            </Animatable.Text>
          )}
        </View>
        <View>
          <Text style={[styles.productName, { color: text }]}>
            Length(mm.) *
          </Text>
          <View style={[styles.view_select_length]}>
            {this.renderPicker()}
            <View></View>
          </View>
          {typeof product.note !== undefined && (
            <Text style={[styles.text_note]}>{product.note}</Text>
          )}
        </View>

        {renderWidth()}
      </View>
    );

    const renderAttributes = () => (
      <View>
        {typeof this.productAttributes !== "undefined" &&
          this.productAttributes.map((attribute, attrIndex) => (
            <View
              key={`attr${attrIndex}`}
              style={[
                styles.productSizeContainer,
                Constants.RTL && { flexDirection: "row-reverse" }
              ]}
            >
              {attribute.name !== Constants.productAttributeColor &&
                attribute.options.map((option, index) => (
                  <ProductAttribute
                    key={index}
                    text={option}
                    style={styles.productSize}
                    onPress={() =>
                      this.onSelectAttribute(attribute.name, option)
                    }
                    selected={
                      attribute.selectedOption.toLowerCase() ===
                      option.toLowerCase()
                    }
                  />
                ))}
            </View>
          ))}
      </View>
    );

    const renderProductColor = () => {
      if (typeof this.productAttributes === "undefined") {
        return;
      }

      const productColor = this.productAttributes.find(
        item => item.name === Constants.productAttributeColor
      );
      if (productColor) {
        const translateY = this.state.scrollY.interpolate({
          inputRange: [0, PRODUCT_IMAGE_HEIGHT / 2, PRODUCT_IMAGE_HEIGHT],
          outputRange: [0, -PRODUCT_IMAGE_HEIGHT / 3, -PRODUCT_IMAGE_HEIGHT],
          extrapolate: "clamp"
        });

        return (
          <Animated.View
            style={[
              styles.productColorContainer,
              { transform: [{ translateY }] }
            ]}
          >
            {productColor.options.map((option, index) => (
              <ProductColor
                key={index}
                color={this.getColor(option)}
                onPress={() =>
                  this.onSelectAttribute(
                    Constants.productAttributeColor,
                    option
                  )
                }
                selected={
                  productColor.selectedOption.toLowerCase() ===
                  option.toLowerCase()
                }
              />
            ))}
          </Animated.View>
        );
      }
    };

    const renderProductRelated = () => (
      <ProductRelated
        onViewProductScreen={product => {
          this.list.getNode().scrollTo({ x: 0, y: 0, animated: true });
          onViewProductScreen(product);
        }}
        product={product}
        // tags={product.related_ids}
      />
    );

    return (
      <View style={[styles.container, { backgroundColor: background }]}>
        <Animated.ScrollView
          ref={c => (this.list = c)}
          overScrollMode="never"
          style={styles.listContainer}
          scrollEventThrottle={1}
          onScroll={event => {
            this.state.scrollY.setValue(event.nativeEvent.contentOffset.y);
          }}
        >
          <View
            style={[styles.productInfo, { backgroundColor: background }]}
            onLayout={event =>
              (this.productInfoHeight = event.nativeEvent.layout.height)
            }
          >
            {this._renderImages()}
            {renderAttributes()}
            {renderTitle()}
            {renderRating()}
          </View>
          {this._renderTabView()}
          {renderProductRelated()}
          <AdMob />
        </Animated.ScrollView>
        {renderProductColor()}

        {renderButtons()}

        <Modal
          ref={com => (this._modalPhoto = com)}
          swipeToClose={false}
          animationDuration={200}
          style={styles.modalBoxWrap}
        >
          <Swiper
            height={Constants.Window.height - 40}
            activeDotStyle={styles.dotActive}
            removeClippedSubviews={false}
            dotStyle={styles.dot}
            paginationStyle={{ zIndex: 9999, bottom: -15 }}
          >
            {product.images.map((image, index) => (
              <Image
                key={index.toString()}
                source={{ uri: getProductImage(image.src, Styles.width) }}
                style={styles.imageProductFull}
              />
            ))}
          </Swiper>

          <TouchableOpacity style={styles.iconZoom} onPress={this.closePhoto}>
            <Text style={styles.textClose}>{Languages.close}</Text>
          </TouchableOpacity>
        </Modal>

        <Modal
          ref={com => (this._modalTest = com)}
          swipeToClose={false}
          animationDuration={200}
          style={styles.modalBoxWrap}
        >
          <View>
            <Text style={[styles.productName, { color: text }]}>Email</Text>
            <TextInput
              style={{
                margin: 5,
                height: 30,
                borderColor: "#000000",
                borderWidth: 1
              }}
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              onChangeText={text => this.setState({ emailsend: text })}
            />
          </View>

          <View>
            <Text style={[styles.productName, { color: text }]}>
              Product Require
            </Text>
            <TextInput
              style={{
                margin: 5,
                height: 30,
                borderColor: "#000000",
                borderWidth: 1
              }}
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              onChangeText={text => this.setState({ productsend: text })}
              value={product.name}
            />
          </View>

          <View>
            <TouchableOpacity onPress={this.submitComment} style={css.sendView}>
              <Icon
                name="cursor"
                size={16}
                color="white"
                style={css.sendButton}
              />
              <Text style={css.sendText}>{Languages.send}</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.iconZoom} onPress={this.closeTest}>
            <Text style={styles.textClose}>{Languages.close}</Text>
          </TouchableOpacity>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    cartItems: state.carts.cartItems,
    wishListItems: state.wishList.wishListItems,
    productVariations: state.products.productVariations,
    userData: state.user.user
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const CartRedux = require("@redux/CartRedux");
  const WishListRedux = require("@redux/WishListRedux");
  const ProductRedux = require("@redux/ProductRedux");
  return {
    ...ownProps,
    ...stateProps,
    addCartItem: (product, variation, width, length, priceItems, option_id) => {
      CartRedux.actions.addCartItem(
        dispatch,
        product,
        variation,
        width,
        length,
        priceItems,
        option_id
      );
    },
    addWishListItem: product => {
      WishListRedux.actions.addWishListItem(dispatch, product);
    },
    removeWishListItem: product => {
      WishListRedux.actions.removeWishListItem(dispatch, product);
    },
    getProductVariations: product => {
      ProductRedux.actions.getProductVariations(dispatch, product);
    }
  };
}

export default withTheme(
  connect(mapStateToProps, undefined, mergeProps)(Detail)
);
