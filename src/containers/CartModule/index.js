/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, WebView, Text, TextInput, TouchableOpacity } from "react-native";
import ScrollableTabView from "react-native-scrollable-tab-view";
import { connect } from "react-redux";
import { isObject } from "lodash";
import * as OpencartWorker from '@services/OpencartWorker'
import { BlockTimer, warn, toast } from "@app/Omni";
import { StepIndicator } from "@components";

import {
  Languages,
  Images,
  AppConfig,
  Constants,
  Config,
  withTheme,
  Alert,
} from "@common";

import MyCart from "./MyCart";
import Delivery from "./Delivery";
import Payment from "./Payment";
import FinishOrder from "./FinishOrder";
import PaymentEmpty from "./Empty";
import Buttons from "./Buttons";
import styles from "./styles";
import css from "./cssstyles";
import Icon from "@expo/vector-icons/SimpleLineIcons";
import Modal from "react-native-modalbox";
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

class Cart extends PureComponent {
  static propTypes = {
    user: PropTypes.object,
    onMustLogin: PropTypes.func.isRequired,
    finishOrder: PropTypes.func.isRequired,
    onBack: PropTypes.func.isRequired,
    navigation: PropTypes.object.isRequired,
    onFinishOrder: PropTypes.func.isRequired,
    onViewProduct: PropTypes.func,
    cartItems: PropTypes.array,
    onViewHome: PropTypes.func,
  };

  static defaultProps = {
    cartItems: [],
  };

  constructor(props) {
    super(props);

    this.state = {
      currentIndex: 0,
      // createdOrder: {},
      userInfo: null,
      order: "",
      isLoading: false,
      orderId: null,
      emailsend: '',
      productsend: '',
      address_data: '',
      updateTextInput: false,

    };


  }

  componentWillMount() {

    this.props.navigation.setParams({ title: Languages.ShoppingCart });

    //console.log('cart module')

    OpencartWorker.getUserInfo()
      .then((res) => {

        console.log('getUserInfo data', res.address)
        //login(res.user, res.token);
        //console.log('LenghProduct data', res.name)
        this.setState({
          addressinfo: res.address,
          user_data: res
        });

      })
      .catch((message) => {
        console.log('getUserInfo data', message)
      })




  }



  componentWillReceiveProps(nextProps) {
    // reset current index when update cart item
    if (this.props.cartItems && nextProps.cartItems) {
      if (nextProps.cartItems.length !== 0) {
        if (this.props.cartItems.length !== nextProps.cartItems.length) {
          this.updatePageIndex(0);
          this.onChangeTabIndex(0);
        }
      }
    }

    //if (nextProps.type == "GET_SHIPPING_METHOD_SUCCESS") {
    //get payment methods
    //this.props.fetchPayments(this.state.userInfo, this.props.token)
    //}
  }


  checkUserLogin = async () => {
    try {
      this.setState({ isLoading: true })
      await OpencartWorker.getUserInfo()
      this.setState({ isLoading: false })
      return true
    } catch (error) {
      this.setState({ isLoading: false })
      return false
    }
  };

  closeTest = () => {
    this._modalTest.close();
  };

  /* openTest = () => {
    this._modalTest.open();
  }; */

  onNextConfirm = async () => {// click button Next Step
    //console.log('toppp555')

    let valid = true;
    switch (this.state.currentIndex) {
      case 0:
        {
          valid = await this.checkUserLogin();
          if (valid) {

            const payload = {
              carts: this.props.cartItems,
              address: this.state.address_data ? this.state.address_data : this.state.address,
              taxid: this.state.taxid
            };

            //console.log('CartModule', this.props.cartItems);

            /*  this.setState({
 
               loading: true,
 
 
             }); */

            OpencartWorker.addNewOrder(
              payload,
              () => {
                this._modalTest.close();
                this.setState({ loading: false });
                this.props.emptyCart();



                //this.props.onNext();
              },
              (message) => {
                alert(message)
                this.setState({ loading: false });
              }
            );

            console.log('confirmOrder')

            //add items to cart
            /*this.props.createAQuote(this.props.cartItems, this.props.token)
            this.props.cartItems.map(function (value, index) {
               OpencartWorker.addItemsToCart(value.product, '1')
                 .then((items) => {
                   console.log('addItemsToCart api', items)
                 })
                 .catch((err) => {
                   console.log('addItemsToCart api', err)
                 })
           })*/

          } else {


            this.props.onMustLogin();
          }
        }
        break;
      /*
    case 1:
      {
        //get shipping methods
        this.props.getShippingMethod(this.state.userInfo, this.props.token)
      }
      break;
      */
      default:
        break;
    }
    if (valid && typeof this.tabCartView !== "undefined") {
      const nextPage = this.state.currentIndex + 1;
      this.tabCartView.goToPage(nextPage);

    }
  };

  OpenModal = () => {

    console.log('OpenModal', this.state.user_data)

    this._modalTest.open();

    this.setState({
      address_data: this.state.addressinfo,
      bgcolor: '#eeeff0',
      updateTextInput: false,
    });

    console.log('this.checkUserLogin()', this.state.user_data)


  };

  /*   SubmitOrder = () => {
      console.log('submit email')
      OpencartWorker.postRequire2(this.props.cartItems, this.state.emailsend)
        .then(() => {
  
          toast("Send Data Successful");
          this._modalTest.close();
        })
        .catch((err) => {
          alert(err)
        })
    };
   */

  onPrevious = () => {

    if (this.state.currentIndex === 0) {
      this.props.onBack();
      return;
    }
    this.tabCartView.goToPage(this.state.currentIndex - 1);
  };

  updatePageIndex = (page) => {
    this.setState({ currentIndex: isObject(page) ? page.i : page });
  };

  onChangeTabIndex = (page) => {
    if (this.tabCartView) {
      this.tabCartView.goToPage(page);
    }
  };

  finishOrder = () => {
    const { onFinishOrder } = this.props;
    onFinishOrder();
    BlockTimer.execute(() => {
      this.tabCartView.goToPage(0);
    }, 1500);
  };

  render() {

    var radio_props = [
      { label: 'same as delivery', value: 0 },
      { label: 'difference', value: 1 }
    ];

    console.log('address radioItems', this.state.radioItems, ' ', this.state.chklogin)

    /* valid = this.checkUserLogin();
    if (valid) {

      console.log('login')
      

    } else {

      console.log('not login')
    } */
    /*  console.log('Address ', this.state.address_data, ' loading', this.checkUserLogin()) */

    if (this.state.radioItems === 0) {

      this.setState({
        address_data: this.state.addressinfo,
        bgcolor: '#eeeff0',
        updateTextInput: false,

      });

    } else if (this.state.radioItems === 1) {

      this.setState({
        address_data: '',
        updateTextInput: true,
        bgcolor: 'white'

      });

    } else {

      this.setState({
        address_data: this.state.addressinfo,
        bgcolor: '#eeeff0',
        updateTextInput: false,
      });

    }

    const { onViewProduct, navigation, cartItems, onViewHome } = this.props;
    const { currentIndex } = this.state;
    const {
      theme: {
        colors: { background },
      },
    } = this.props;

    if (currentIndex === 0 && cartItems && cartItems.length === 0) {
      return <PaymentEmpty onViewHome={onViewHome} />;
    }
    const steps = [
      { label: Languages.MyCart, icon: Images.IconCart },
      { label: Languages.Delivery, icon: Images.IconPin },
      { label: Languages.Payment, icon: Images.IconMoney },
      { label: Languages.Order, icon: Images.IconFlag },
    ];
    return (

      <View style={[styles.fill, { backgroundColor: background }]}>
        <View style={styles.content}>
          <ScrollableTabView
            ref={(tabView) => {
              this.tabCartView = tabView;
            }}
            locked
            onChangeTab={this.updatePageIndex}
            style={{ backgroundColor: background }}
            initialPage={0}
            tabBarPosition="overlayTop"
            prerenderingSiblingsNumber={1}
            renderTabBar={() => <View style={{ padding: 0, margin: 0 }} />}>
            <MyCart
              key="cart"
              navigation={navigation}
              onViewProduct={onViewProduct}
            />

            <FinishOrder key="finishOrder" finishOrder={this.finishOrder} />
          </ScrollableTabView>

          {currentIndex === 0 && (

            <Buttons onPrevious={this.onPrevious} nextText={Languages.ConfirmOrder}
              onNext={this.OpenModal} isLoading={this.state.isLoading} />
          )}
        </View>

        <Modal
          ref={(com) => (this._modalTest = com)}
          swipeToClose={false}
          animationDuration={200}
          style={styles.modalBoxWrap}>
          <View style={{ marginTop: 75, }}>
            <Text style={{ marginLeft: 5, }}>Tax ID</Text>
            <TextInput style={{
              margin: 5,
              height: 30,
              borderColor: "#000000",
              borderWidth: 1
            }}
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              onChangeText={(text) => this.setState({ taxid: text })} />

            <Text style={{ marginLeft: 5, }}>{Languages.Address}</Text>

            <RadioForm
              radio_props={radio_props}
              initial={0}
              onPress={(value) => { this.setState({ radioItems: value }) }}
              style={{ marginLeft: 5, }}
            />

            <TextInput style={{
              margin: 5,
              height: 30,
              borderColor: "#000000",
              borderWidth: 1
            }}
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              numberOfLines={10}
              multiline={true}
              onChangeText={(text) => this.setState({ address: text })}
              value={this.state.address_data ? this.state.address_data : this.state.address}
              returnKeyType={"next"}
              editable={this.state.updateTextInput}
              underlineColorAndroid='transparent'
              backgroundColor={this.state.bgcolor}
            /* style={{ backgroundColor: '#F9F7F6' }}
            borderColor={'#b76c94'} */

            /* onChangeText={(text) => this.setState({ emailsend: text })} */
            />
          </View>

          <View>
            <TouchableOpacity onPress={this.onNextConfirm} style={css.sendView}>
              {/* <Icon
                name="cursor"
                size={16}
                color="white"
                style={css.sendButton}
              /> */}
              <Text style={css.sendText}>{Languages.Confirm}</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.iconZoom} onPress={this.closeTest}>
            <Text style={styles.textClose}>{Languages.close}</Text>
          </TouchableOpacity>
        </Modal>
      </View>

    );
  }
}

const mapStateToProps = ({ carts, user, addresses }) => ({
  cartItems: carts.cartItems,
  type: carts.type,
  user,
  token: user.token,
  selectedAddress: addresses.selectedAddress,
});
function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const CartRedux = require("@redux/CartRedux");
  const PaymentRedux = require("@redux/PaymentRedux")
  const { actions } = require("@redux/UserRedux");
  return {
    ...ownProps,
    ...stateProps,
    emptyCart: () => CartRedux.actions.emptyCart(dispatch),
    finishOrder: () => CartRedux.actions.finishOrder(dispatch),
    createAQuote: (carts, token) => CartRedux.actions.createAQuote(dispatch, carts, token),
    getShippingMethod: (address, token) => {
      CartRedux.actions.getShippingMethod(dispatch, address, token);
    },
    fetchPayments: (address, token) => {
      PaymentRedux.actions.fetchPayments(dispatch, address, token);
    },
    logout: () => dispatch(actions.logout()),
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(Cart));
