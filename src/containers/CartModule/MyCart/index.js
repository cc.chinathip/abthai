import React, { PureComponent } from "react";
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { connect } from "react-redux";
import { SwipeRow } from "react-native-swipe-list-view";
import FontAwesome from "@expo/vector-icons/FontAwesome";
import { LinearGradient } from "@expo";
import { toast } from "@app/Omni";
import { ProductItem } from "@components";
import { Languages, Color, withTheme, Tools } from "@common";
import css from "@cart/styles";
import styles from "./styles";

class MyCart extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      coupon: props.couponCode,
      qty: 0
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.hasOwnProperty("type") &&
      nextProps.type == "GET_COUPON_CODE_FAIL"
    ) {
      toast(nextProps.message);
    }
  }
  getResponse(qty) {
    this.setState({ qty });
  }
  render() {

    const { cartItems, totalPrice, isFetching, discountType } = this.props;
    const {
      theme: {
        colors: { text, lineColor },
        dark,
      },
    } = this.props;

    let couponBtn = Languages.ApplyCoupon;
    let colors = [Color.darkOrange, Color.darkYellow, Color.yellow];
    console.log('totalPrice', this.getExistCoupon())
    const finalPrice =
      discountType == "percent"
        ? totalPrice - this.getExistCoupon() * totalPrice
        : totalPrice - this.getExistCoupon();

    if (isFetching) {
      couponBtn = Languages.ApplyCoupon;
    } else if (this.getExistCoupon() > 0) {
      colors = [Color.darkRed, Color.red];
      couponBtn = Languages.remove;
    }

    //console.log(' price=', cartItems.product.price, ' qty=', cartItems.quantity)
    //console.log('cartItems4445455', cartItems)


    //console.log('qtyqtyqtyqty', cartItems)

    var total = 0;
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={css.row}>
            <Text style={[css.label, { color: text }]}>
              {Languages.MyCart}
            </Text>
            {
              cartItems.map(function (value, index) {
                //total += (value.quantity * value.product.sale_price)
                total += (value.quantity * Tools.getCurrecyFormatted(value.priceItems))
                console.log('value.quantity', value.quantity)
              })
            }
            <Text style={css.value}>{Tools.getCurrecyFormatted(total)}</Text>

          </View>
          <View style={styles.list}>
            {cartItems &&
              cartItems.map((item, index) => (

                <SwipeRow
                  key={index.toString()}
                  disableRightSwipe
                  leftOpenValue={75}
                  rightOpenValue={-75}>
                  {this.renderHiddenRow(item, index)}
                  <ProductItem
                    key={index.toString()}
                    viewQuantity
                    product={item.product}
                    onPress={() =>
                      this.props.onViewProduct({ product: item.product })
                    }
                    width={item.width}
                    length={item.length}
                    option_id={item.option_id}
                    width_option_id={item.width_option_id}
                    quantity={item.quantity}
                    onRemove={this.props.removeCartItem}
                    priceItems={item.priceItems}
                    parentCallback={this.getResponse.bind(this)}

                  />
                </SwipeRow>
              ))}
          </View>
        </ScrollView>
      </View>
    );
  }

  renderHiddenRow = (rowData, index) => {

    return (
      <TouchableOpacity
        key={`hiddenRow-${index}`}
        style={styles.hiddenRow}
        onPress={() =>
          /*  product, variation, width, length, priceItems */
          this.props.removeCartItem(rowData.product, rowData.variation, rowData.width, rowData.length, rowData.priceItems, rowData.option_id, rowData.width_option_id)

        }>
        <View style={{ marginRight: 23 }}>
          <FontAwesome name="trash" size={30} color="white" />
        </View>
      </TouchableOpacity>
    );
  };

  checkCouponCode = () => {
    if (this.getExistCoupon() == 0) {
      this.props.getCouponAmount(this.state.coupon);
    } else {
      this.props.cleanOldCoupon();
    }
  };

  getCouponString = () => {
    const { discountType } = this.props;
    const couponValue = this.getExistCoupon();
    if (discountType == "percent") {
      return `${couponValue * 100}%`;
    }
    return Tools.getCurrecyFormatted(couponValue);
  };

  getExistCoupon = () => {
    const { couponCode, couponAmount, discountType } = this.props;
    if (couponCode == this.state.coupon) {
      if (discountType == "percent") {
        return couponAmount / 100.0;
      }
      return couponAmount;
    }
    return 0;
  };
}

MyCart.defaultProps = {
  couponCode: "",
  couponAmount: 0,
};

const mapStateToProps = ({ carts, products }) => {
  return {
    cartItems: carts.cartItems,
    totalPrice: carts.totalPrice,
    couponCode: products.coupon && products.coupon.code,
    couponAmount: products.coupon && products.coupon.amount,
    discountType: products.coupon && products.coupon.type,

    isFetching: products.isFetching,
    type: products.type,
    message: products.message,
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/CartRedux");
  const productActions = require("@redux/ProductRedux").actions;
  return {
    ...ownProps,
    ...stateProps,
    /* removeCartItem: (product, variation) => {
      actions.removeCartItem(dispatch, product, variation);
    }, */

    removeCartItem: (product, variation, width, length, priceItems, option_id, width_option_id) => {
      actions.removeCartItem(dispatch, product, variation, width, length, priceItems, option_id, width_option_id);
    },
    cleanOldCoupon: () => {
      productActions.cleanOldCoupon(dispatch);
    },
    getCouponAmount: (coupon) => {
      productActions.getCouponAmount(dispatch, coupon);
    },
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(MyCart));