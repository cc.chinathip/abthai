/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { FlatList, View, Text, Image, TouchableHighlight } from "react-native";
import { Constants, Images, Config, Languages, withTheme, AppConfig } from "@common";
import { HorizonLayout, AdMob, BannerSlider, BannerImage, PostList } from "@components";
import { find } from "lodash";
import styles from "./styles";
import Categories from "./Categories";
import HHeader from "./HHeader";
import * as OpencartWorker from '../../services/OpencartWorker'
import { NavigationActions } from "react-navigation";
import Login from '../../navigation/LoginScreen';

class HorizonList extends PureComponent {
  static propTypes = {
    config: PropTypes.object,
    index: PropTypes.number,
    fetchPost: PropTypes.func,
    onShowAll: PropTypes.func,
    list: PropTypes.array,
    fetchProductsByCollections: PropTypes.func,
    setSelectedCategory: PropTypes.func,
    onViewProductScreen: PropTypes.func,
    showCategoriesScreen: PropTypes.func,
    collection: PropTypes.object,
    onMove: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.page = 1;
    this.limit = Constants.pagingLimit;
    this.defaultList = [
      {
        id: 1,
        name: Languages.loading,
        images: [Images.PlaceHolder],
      },
      {
        id: 2,
        name: Languages.loading,
        images: [Images.PlaceHolder],
      },
      {
        id: 3,
        name: Languages.loading,
        images: [Images.PlaceHolder],
      },
    ];
    this.state = {
      CategoriesAll: null
    }

  }
  /* 
    async componentDidMount() {
  
      OpencartWorker.getCategories()
        .then((items) => {
  
          this.setState({
            CategoriesAll: items
          })
  
        })
        .catch((err) => {
          //console.log('OpencartWorker.getCategories()', err)
  
        })
  
    } */


  /**
   * handle load more
   */

  _nextPosts = () => {
    const { config, index, fetchPost, collection } = this.props;
    this.page += 1;
    if (!collection.finish) {
      fetchPost({ config, index, page: this.page });
    }
  };

  _viewAll = () => {
    const {
      config,
      onShowAll,
      index,
      list,
      fetchProductsByCollections,
      setSelectedCategory,
    } = this.props;
    const selectedCategory = find(
      list,
      (category) => category.id === config.category

    );

    setSelectedCategory(selectedCategory);
    fetchProductsByCollections(config.category, config.tag, this.page, index);
    onShowAll(config, index);
  };

  showProductsByCategory = (config) => {

    const {
      onShowAll,
      index,
      list,
      fetchProductsByCollections,
      setSelectedCategory,
    } = this.props;
    const selectedCategory = find(
      list,
      (category) => category.id === config.category
    );
    setSelectedCategory(selectedCategory);
    fetchProductsByCollections(config.category, config.tag, this.page, index);
    onShowAll(config, index);
  };

  onViewProductScreen = (product) => {
    this.props.onViewProductScreen({ product });

  };

  renderItem = ({ item, index }) => {
    const { layout } = this.props.config;
    //console.log('layout config', layout)

    if (item === null) return <View key="post_" />;
    //console.log('PropTypes.object', this.defaultList)
    //console.log('item', item)
    return (
      <HorizonLayout
        product={item}
        key={`post-${index}`}
        onViewPost={this.onViewProductScreen}
        layout={layout}
      />
    );
  };

  renderHeader = () => {
    const { showCategoriesScreen, config, theme, onMove } = this.props;
    return (
      <HHeader
        showCategoriesScreen={showCategoriesScreen}
        config={config}
        theme={theme}
        viewAll={this._viewAll}
        onMove={onMove}
      />
    );
  };


  render() {

    pull = () => {

      console.log('pull')

      this.onMove;

      //this.props.navigate('LoginScreen');
    };

    /*    onSignUpHandle = () => {
         this.props.onViewSignUp();
       };
    */
    //const { navigate } = this.props.navigation;
    /*     var renderCategoriesAll = [];
    
        if (this.state.CategoriesAll !== null) {
          this.state.CategoriesAll.map(function (value, index) {
            //console.log('i', value.image.src)
            if (value.image.src !== '') {
              renderCategoriesAll.push(
                <View style={styles.container_category} key={index} >
    
                  <View style={{ marginVertical: 20 }}><Text>{value.name}</Text></View>
                  <TouchableHighlight onPress={this.pull}
                  //this.props.navigate('LoginScreen')
                  >
                    <Image style={{ width: 150, height: 150 }} source={{ uri: value.image.src }} />
                  </TouchableHighlight>
                </View>);
            }
    
          });
        } */

    const {
      onViewProductScreen,
      collection,
      config,
      isLast,
      theme: {
        colors: { text },
      },
    } = this.props;

    const { VerticalLayout } = AppConfig

    const list =
      typeof collection !== "undefined" ? collection.list : this.defaultList;
    const isPaging = !!config.paging;


    switch (config.layout) {
      case Constants.Layout.circleCategory:
        return (
          <Categories
            config={config}
            categories={this.props.list}
            items={Config.HomeCategories}
            type={config.theme}
            onPress={this.showProductsByCategory}
          />
        );
      case Constants.Layout.BannerSlider:
        return (
          <BannerSlider data={list} onViewPost={this.onViewProductScreen} />
        );

      case Constants.Layout.BannerImage:
        return (
          <BannerImage
            viewAll={this._viewAll}
            config={config}
            data={list}
            onViewPost={this.onViewProductScreen}
          />
        );
    }
    //console.log('list', list)
    //config.name && this.renderHeader() หัวข้อและปุ่มทั้งหมด
    /*  if (this.state.CategoriesAll !== null) {
       this.state.CategoriesAll.map(function (value, index) {
         console.log('this.state.CategoriesAll', index)
       });
     } */
    console.log('this.renderHeader()', this.renderHeader())



  }
}

export default withTheme(HorizonList);

