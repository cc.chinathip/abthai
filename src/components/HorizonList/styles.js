/** @format */

import { StyleSheet, Platform, Dimensions } from "react-native";
import { Constants } from "@common";

const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
  width: Dimensions.get("window").width,
  flatWrap: {
    flex: 1,
    paddingLeft: 0,
    marginTop: 50,
    /*  marginBottom: 15, */
    /*  flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start" */
    /* margin: 5 */
    width
  },
  flatWrapOld: {
    flex: 1,
    paddingLeft: 0,
    marginTop: 50,
    /*  marginBottom: 15, */
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start"
  },
  flatlist: {
    flexDirection: "row"
  },
  mainList: {
    flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: 20,

    ...Platform.select({
      ios: {
        paddingTop: 30
      },
      android: {
        paddingTop: 60
      }
    })
  },
  flatVertical: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    backgroundColor: "#FFF"
  },
  isListing: {
    marginTop: 60
  },
  more: {
    width,
    alignItems: "center",
    marginBottom: 10,
    marginTop: 10
  },
  spinView: {
    width,
    backgroundColor: "#fff",
    flex: 1,
    height,
    paddingTop: 20
  },
  view_banner: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10
  },
  view_name_banner: {
    justifyContent: "center",
    alignItems: "center"
  },
  text_name_banner: {
    fontSize: 20,
    fontWeight: "bold"
  },
  image_banner: {
    width,
    height: 100,
    margin: 10
  },
  image_product_cate: {
    width,
    height: 100,
    alignItems: "center",
    justifyContent: "center",
    margin: 10
    /*  resizeMode: "cover" */
  },
  product_cate: {
    width: "100%",
    height: 150,
    alignItems: "center",
    justifyContent: "center",
    resizeMode: "contain"
    /* margin: 10 */
  },
  viewbannerproductcate: {
    alignItems: "center",
    justifyContent: "center"
  },
  banenr_category: {
    width: "100%",
    height: 100,
    margin: 10,
    resizeMode: "cover"
  },
  view_product: {
    alignItems: "center",
    width: "50%"
  },
  image_product: {
    width: 170,
    height: 150,
    borderWidth: 2,
    /*  borderColor: '#d6d7da' */
    borderColor: "black"
  },
  navbar: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    alignItems: "center",
    zIndex: 9999,
    backgroundColor: "transparent",
    // borderBottomColor: 'none',
    // borderBottomWidth: 1,
    height: 40,
    justifyContent: "center"
  },
  contentContainer: {
    paddingTop: 40
  },
  title: {
    color: "#333333"
  },
  row: {
    height: 300,
    width: null,
    marginBottom: 1,
    padding: 16,
    backgroundColor: "transparent"
  },
  rowText: {
    color: "white",
    fontSize: 18
  },
  transparentTop: {
    backgroundColor: "transparent"
  },
  // RenderHedearListView
  header: {
    flexDirection: "row",
    marginBottom: 12,
    marginTop: 18
  },
  headerLeft: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-start",
    marginLeft: 15
  },
  headerRight: {
    flex: 1 / 3,
    justifyContent: "flex-end",
    alignItems: "center",
    marginRight: 0,
    flexDirection: "row"
  },
  headerRightText: {
    fontSize: 11,
    marginRight: 0,
    marginTop: 0,
    color: "#666",
    fontFamily: Constants.fontFamily
  },
  icon: {
    marginRight: 8,
    marginTop: 2,
    backgroundColor: "transparent"
  },
  icon_style_more: {
    alignSelf: "flex-end",
    marginRight: 10,
    position: "absolute",
    right: 0
  },

  tagHeader: {
    fontSize: 18,
    color: "#666",
    letterSpacing: 2,
    fontFamily: Constants.fontHeader
  },

  headerLogo: {
    alignItems: "center",
    width: width,

    ...Platform.select({
      ios: {
        paddingTop: 60
      },
      android: {
        paddingTop: 80
      }
    })
  },
  headerDate: {
    fontSize: 14,
    paddingTop: 5,
    marginBottom: 0,
    fontWeight: "400",
    opacity: 0.8,
    fontFamily: Constants.fontFamily
  },
  headerStore: {
    color: "#333",
    fontSize: 30,
    marginBottom: 10,
    fontFamily: Constants.fontFamily
  },
  logo: {
    height: 50,
    width: 150,
    resizeMode: "contain"
  },
  card: {
    borderWidth: 3,
    borderRadius: 3,
    borderColor: "#000",
    width: 300,
    height: 300,
    padding: 10
  },
  container_category: {
    /*   flex: 1,
      alignItems: 'center',
      justifyContent: 'center', */
    alignItems: "center",
    width,
    borderWidth: 0.5,
    borderColor: "#d6d7da"
  },
  list_item_product: {
    borderBottomColor: "black",
    borderBottomWidth: 2,
    marginBottom: 30,
    width
    /*  height */
  },
  text_item: {
    marginVertical: 10,
    marginLeft: 15
  },
  BannerHome: {
    width: width,
    height: 200,
    marginTop: 50
  }
});
