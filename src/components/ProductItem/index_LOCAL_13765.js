/** @format */

import React, { PureComponent } from "react";
import { TouchableOpacity, Text, View, Image, Dimensions } from "react-native";
import ChangeQuantity from "@components/ChangeQuantity";
import { connect } from "react-redux";
import { withTheme, Tools } from "@common";
import styles from "./styles";

class ProductItem extends PureComponent {
  onChangeQuantity = (quantity) => {
    if (this.props.quantity < quantity) {
      console.log('addCartItem ProductItem', this.props)
      this.props.addCartItem(this.props.product, this.props.variation, this.props.width, this.props.length);
    } else {
      this.props.removeCartItem(this.props.product, this.props.variation, this.props.width, this.props.length);
    }
  };

  render() {
    const {
      product,
      quantity,
      viewQuantity,
      variation,
      width,
      length,
      onPress,
      onRemove,
    } = this.props;
    const {
      theme: {
        colors: { background, text, lineColor },
        dark: isDark,
      },
    } = this.props;
    let image_item = product.image;
    return (

      <View
        style={[
          styles.container,
          { backgroundColor: background },
          isDark && { borderBottomColor: lineColor },
        ]}>
        <View style={styles.content}>
          <TouchableOpacity onPress={() => onPress({ product })}>
            <Image
              source={{ uri: Tools.getImageVariation(image_item, variation) }}
              style={styles.image}
            />
          </TouchableOpacity>

          <View
            style={[
              styles.infoView,
              { width: Dimensions.get("window").width - 180 },
            ]}>
            <TouchableOpacity onPress={() => onPress({ product })}>
              <Text style={[styles.title, { color: text }]}>
                {product.name}
              </Text>
            </TouchableOpacity>
            <View style={styles.priceContainer}>
              <Text style={[styles.price, { color: text }]}>
                {Tools.getPriceIncluedTaxAmount(product, variation)}
              </Text>
              {variation &&
                typeof variation.attributes !== "undefined" &&
                variation.attributes.map((variant) => {
                  return (
                    <Text
                      key={variant.name}
                      style={styles.productVariant(text)}>
                      {variant.option}
                    </Text>
                  );
                })}
            </View>
            <Text style={[styles.title, { color: text }]}>
              Width: {width}
            </Text>
            <Text style={[styles.title, { color: text }]}>
              Length: {length}
            </Text>
          </View>
          {viewQuantity && (
            <ChangeQuantity
              style={styles.quantity}
              quantity={quantity}
              onChangeQuantity={this.onChangeQuantity}
            />
          )}
        </View>

        {viewQuantity && (
          <TouchableOpacity
            style={styles.btnTrash}
            onPress={() => onRemove(product, variation)}>
            <Image
              source={require("@images/ic_trash.png")}
              style={[styles.icon, { tintColor: text }]}
            />
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/CartRedux");
  return {
    ...ownProps,
    ...stateProps,
    addCartItem: (product, variation) => {
      actions.addCartItem(dispatch, product, variation);
    },
    removeCartItem: (product, variation) => {
      actions.removeCartItem(dispatch, product, variation);
    },
  };
}

export default connect(
  null,
  undefined,
  mergeProps
)(withTheme(ProductItem));
