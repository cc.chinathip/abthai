/** @format */

import React, { PureComponent } from "react";
import { TouchableOpacity, Text, View, Image, Dimensions } from "react-native";
import ChangeQuantity from "@components/ChangeQuantity";
import { connect } from "react-redux";
import { withTheme, Tools } from "@common";
import styles from "./styles";

class ProductItem extends PureComponent {
  onChangeQuantity = (quantity) => {
    if (this.props.quantity < quantity) {
<<<<<<< HEAD
      console.log('addCartItem ProductItem', this.props)
      this.props.addCartItem(this.props.product, this.props.variation, this.props.width, this.props.length);
    } else {
      this.props.removeCartItem(this.props.product, this.props.variation, this.props.width, this.props.length);
    }
  };
=======
      // เงื่อนไขการเพิ่ม จำนวนสินค้า

      //console.log('add quantity ProductItem', this.props.quantity, ' ', quantity)
      //console.log('product onChangeQuantity', this.props.product, this.props.variation, this.props.width, this.props.length, this.props.priceItems)


      this.props.addCartItem(this.props.product, this.props.variation, this.props.width, this.props.length, this.props.priceItems, this.props.option_id, this.props.width_option_id);



    } else {

      // เงื่อนไขการลด จำนวนสินค้า


      //console.log('remove quantity ProductItem', this.props.quantity, ' ', quantity)

      this.props.removeCartItem(this.props.product, this.props.variation, this.props.width, this.props.length, this.props.priceItems, this.props.option_id, this.props.width_option_id);


    }
  }

  /*  calc() {
     this.props.parentCallback(666);
   } */


>>>>>>> test

  render() {
    const {
      product,
      quantity,
      viewQuantity,
      variation,
      width,
      length,
      onPress,
      onRemove,
<<<<<<< HEAD
=======
      priceItems,
      option_id,
      width_option_id,
>>>>>>> test
    } = this.props;
    const {
      theme: {
        colors: { background, text, lineColor },
        dark: isDark,
      },
    } = this.props;
    let image_item = product.image;
    return (

      <View
        style={[
          styles.container,
          { backgroundColor: background },
          isDark && { borderBottomColor: lineColor },
        ]}>
        <View style={styles.content}>
          <TouchableOpacity onPress={() => onPress({ product })}>
            <Image
              source={{ uri: Tools.getImageVariation(image_item, variation) }}
              style={styles.image}
            />
          </TouchableOpacity>

          <View
            style={[
              styles.infoView,
              { width: Dimensions.get("window").width - 180 },
            ]}>
            <TouchableOpacity onPress={() => onPress({ product })}>
              <Text style={[styles.title, { color: text }]}>
                {product.name}
<<<<<<< HEAD
=======

>>>>>>> test
              </Text>
            </TouchableOpacity>
            <View style={styles.priceContainer}>
              <Text style={[styles.price, { color: text }]}>
                {Tools.getPriceIncluedTaxAmount(product, variation)}
              </Text>
              {variation &&
                typeof variation.attributes !== "undefined" &&
                variation.attributes.map((variant) => {
                  return (
                    <Text
                      key={variant.name}
                      style={styles.productVariant(text)}>
                      {variant.option}
                    </Text>
                  );
                })}
            </View>
            <Text style={[styles.title, { color: text }]}>
              Width: {width}
            </Text>
            <Text style={[styles.title, { color: text }]}>
              Length: {length}
            </Text>
          </View>
          {viewQuantity && (
            <ChangeQuantity
              style={styles.quantity}
              quantity={quantity}
              onChangeQuantity={this.onChangeQuantity}
            />
          )}
        </View>

        {viewQuantity && (
          <TouchableOpacity
            style={styles.btnTrash}
<<<<<<< HEAD
            onPress={() => onRemove(product, variation)}>
            <Image
=======
            /* product, variation, width, length, priceItems */
            onPress={() => onRemove(product, variation, width, length, priceItems, option_id, width_option_id)
            }>
            {/* <Image
>>>>>>> test
              source={require("@images/ic_trash.png")}
              style={[styles.icon, { tintColor: text }]}
            />
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/CartRedux");
  return {
    ...ownProps,
    ...stateProps,
<<<<<<< HEAD
    addCartItem: (product, variation) => {
      actions.addCartItem(dispatch, product, variation);
    },
    removeCartItem: (product, variation) => {
=======
    /*  addCartItem: (product, variation) => {
       actions.addCartItem(dispatch, product, variation);
     }, */


    addCartItem: (product, variation, width, length, priceItems, option_id, width_option_id) => {
      actions.addCartItem(dispatch, product, variation, width, length, priceItems, option_id, width_option_id);
    },
    removeCartItem: (product, variation, width, length, priceItems, option_id, width_option_id) => {
      actions.removeCartItem(dispatch, product, variation, width, length, priceItems, option_id, width_option_id);
    },
    /* removeCartItem: (product, variation) => {
>>>>>>> test
      actions.removeCartItem(dispatch, product, variation);
    },
  };
}

export default connect(
  null,
  undefined,
  mergeProps
)(withTheme(ProductItem));
