/** @format */

import React, { PureComponent } from "react";
import { TouchableOpacity, Text, View, Image, Dimensions, Alert } from "react-native";
import ChangeQuantity from "@components/ChangeQuantity";
import { connect } from "react-redux";
import { withTheme, Tools } from "@common";
import styles from "./styles";

class ProductItem extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      quantity_click: 1,
    };
  }


  onChangeQuantity = (quantity) => {


    if (this.props.quantity < quantity) {
      // เงื่อนไขการเพิ่ม จำนวนสินค้า

      //console.log('add quantity ProductItem', this.props.quantity, ' ', quantity)
      //console.log('product onChangeQuantity', this.props.product, this.props.variation, this.props.width, this.props.length, this.props.priceItems)


      this.props.addCartItem(this.props.product, this.props.variation, this.props.width, this.props.length, this.props.priceItems, this.props.option_id, this.props.width_option_id);



    } else {

      // เงื่อนไขการลด จำนวนสินค้า


      //console.log('remove quantity ProductItem', this.props.quantity, ' ', quantity)

      this.props.removeCartItem(this.props.product, this.props.variation, this.props.width, this.props.length, this.props.priceItems, this.props.option_id, this.props.width_option_id);


    }
  }

  /*  calc() {
     this.props.parentCallback(666);
   } */



  render() {

    //console.log('this.state.quantity_click', this.state.quantity_click)
    const {
      product,
      quantity,
      viewQuantity,
      variation,
      width,
      length,
      onPress,
      onRemove,
      priceItems,
      option_id,
      width_option_id,
    } = this.props;
    const {
      theme: {
        colors: { background, text, lineColor },
        dark: isDark,
      },
    } = this.props;
    let image_item = product.image;
    //console.log('ProductItems priceItems', priceItems)

    console.log('onRemove', onRemove)
    return (

      <View
        style={[
          styles.container,
          { backgroundColor: background },
          isDark && { borderBottomColor: lineColor },
        ]}>
        <View style={styles.content}>
          <TouchableOpacity onPress={() => onPress({ product })}>
            <Image
              source={{ uri: Tools.getImageVariation(image_item, variation) }}
              style={styles.image}
            />
          </TouchableOpacity>

          <View
            style={[
              styles.infoView,
              { width: Dimensions.get("window").width - 180 },
            ]}>
            <TouchableOpacity onPress={() => onPress({ product })}>
              <Text style={[styles.title, { color: text }]}>
                {product.name}

              </Text>
            </TouchableOpacity>
            <View style={styles.priceContainer}>
              <Text style={[styles.price, { color: text }]}>
                {/*  {Tools.getPriceIncluedTaxAmount(product, variation)} */}
                {Tools.getCurrecyFormatted(priceItems)}
              </Text>
              {variation &&
                typeof variation.attributes !== "undefined" &&
                variation.attributes.map((variant) => {
                  return (
                    <Text
                      key={variant.name}
                      style={styles.productVariant(text)}>
                      {variant.option}
                    </Text>
                  );
                })}
            </View>
            <Text style={[styles.title, { color: text }]}>
              Width: {width}
            </Text>
            <Text style={[styles.title, { color: text }]}>
              Length: {length}
            </Text>
          </View>
          {viewQuantity && (
            <ChangeQuantity
              style={styles.quantity}
              quantity={quantity}
              onChangeQuantity={this.onChangeQuantity}
            />
          )}
        </View>

        {viewQuantity && (

          <TouchableOpacity
            style={styles.btnTrash}
            /* product, variation, width, length, priceItems */
            onPress={() => onRemove(product, variation, width, length, priceItems, option_id, width_option_id)
            }>
            {/* <Image
              source={require("@images/ic_trash.png")}
              style={[styles.icon, { tintColor: text }]}
            /> */}
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/CartRedux");
  return {
    ...ownProps,
    ...stateProps,
    /*  addCartItem: (product, variation) => {
       actions.addCartItem(dispatch, product, variation);
     }, */


    addCartItem: (product, variation, width, length, priceItems, option_id, width_option_id) => {
      actions.addCartItem(dispatch, product, variation, width, length, priceItems, option_id, width_option_id);
    },
    removeCartItem: (product, variation, width, length, priceItems, option_id, width_option_id) => {
      actions.removeCartItem(dispatch, product, variation, width, length, priceItems, option_id, width_option_id);
    },
    /* removeCartItem: (product, variation) => {
      actions.removeCartItem(dispatch, product, variation);
    }, */
  };
}

export default connect(
  null,
  undefined,
  mergeProps
)(withTheme(ProductItem));
