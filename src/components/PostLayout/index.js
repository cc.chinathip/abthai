/** @format */

import React, { PureComponent } from "react";
import { Constants, Tools, Styles } from "@common";
import { getProductImage } from "@app/Omni";

import ReadMoreLayout from "./ReadMore";
import ColumnLayout from "./Column";
import ThreeColumn from "./ThreeColumn";
import CardLayout from "./Card";
import SimpleLayout from "./Simple";

export default class PostLayout extends PureComponent {
  viewCategoryDetail(id) {
    Tools.viewCateDetail(id);
    this.props.navigate("Default");
  }

  render() {
    const data = this.props.post;
    const { onViewPost, type } = this.props;
    const isProduct = type == "undefined";

    let image_width = 0;
    let imageURL = "";
    let imageURL_one = "";

    const categories = this.props.categories ? this.props.categories : 1;
    const cate =
      typeof data.categories !== "undefined" ? data.categories[0] : 1;
    let postTitle = typeof data.name === "undefined" ? "" : data.name;

    if (typeof type !== "undefined") {
      // news type
      imageURL = Tools.getImage(data, Constants.PostImage.large);
      postTitle =
        typeof data.title !== "undefined"
          ? Tools.getDescription(data.title.rendered, 300)
          : "";


    } else {
      // product type
      image_width = Constants.Layout.card
        ? Styles.width
        : Styles.width * 0.45 - 2;
      imageURL =
        typeof data.images !== "undefined"
          ? getProductImage(data.images[0].src, image_width)
          : "";


      imageURL_one = typeof data.image !== "undefined"
        ? getProductImage(data.image, image_width)
        : "";

      //console.log('imageURL_one', imageURL_one)

    }

    switch (this.props.layout) {
      case Constants.Layout.simple:
        return (
          <SimpleLayout
            imageURL={imageURL_one}
            title={Tools.getDescription(postTitle, 100)}
            viewPost={onViewPost}
            post={data}
            type={type}
            category={categories[cate]}
            date={type ? data.date : data.date_created}
          />
        );

      case Constants.Layout.card:
        return (
          <CardLayout
            imageURL={imageURL_one}
            title={Tools.getDescription(postTitle, 300)}
            viewPost={onViewPost}
            post={data}
            type={type}
            date={type ? data.date : data.date_created}
          />
        );

      case Constants.Layout.twoColumn:
        return (

          <ColumnLayout
            imageURL={imageURL_one}
            title={Tools.getDescription(postTitle)}
            viewPost={onViewPost}
            post={data}
            type={type}
            date={type ? data.date : data.date_created}
          />
        );

      case Constants.Layout.threeColumn:
        imageURL = isProduct
          ? getProductImage(data.image, Styles.width / 3)
          : getProductImage(data.image, Styles.width / 3);
        return (
          <ThreeColumn
            imageURL={imageURL}
            title={Tools.getDescription(postTitle)}
            viewPost={onViewPost}
            post={data}
            type={type}
            date={type ? data.date : data.date_created}
          />
        );

      case Constants.Layout.list:
        return (
          <ReadMoreLayout
            imageURL={imageURL_one}
            title={Tools.getDescription(postTitle)}
            viewPost={onViewPost}
            post={data}
            type={type}
            date={type ? data.date : data.date_created}
          />
        );

      default:
        imageURL = isProduct
          ? getProductImage(data.image, Styles.width / 3)
          : getProductImage(data.image, Styles.width / 3);
        return (
          <ThreeColumn
            imageURL={imageURL}
            title={Tools.getDescription(postTitle)}
            viewPost={onViewPost}
            post={data}
            type={type}
            date={type ? data.date : data.date_created}
          />
        );
    }
  }
}
