import * as Localization from 'expo-localization';
import i18n from 'i18n-js';

i18n.translations = {
    en: { welcome: 'Hello', name: 'chi' },
    th: { welcome: 'สวัสดี', name: 'ชิ' },
};

i18n.locale = Localization.locale;
// When a value is missing from a language it'll fallback to another language with the key present.
i18n.fallbacks = true;

/* function App() {
    return (

        {i18n.t('welcome')} {i18n.t('name')}

    );
  } */