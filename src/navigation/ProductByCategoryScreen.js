/** @format */

import React, { PureComponent } from "react";
import { Back, EmptyView, Logo, LogoPageSubCate } from "./IconNav";
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  FlatList,
  ScrollView,
  Dimensions
} from "react-native";
import { Color, Styles, withTheme, Images } from "@common";
import * as OpencartWorker from "../services/OpencartWorker";
import styles from "../components/HorizonList/styles";
import Icon from "react-native-vector-icons/Entypo";
export default class ProductByCategoryScreen extends PureComponent {
  constructor(props) {
    super(props);
    const params = this.props.navigation.state.params;
    this.state = {
      getcategory_id: params ? params.category_id : null,
      ProductAll: null,
      getcategory_name: params ? params.category_name : null,
      getcategory_image: params ? params.category_image : null,
      getcategory_sub_image: params ? params.category_sub_image : null,
      test: ""
    };
  }

  static navigationOptions = ({ navigation }) => {
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );
    const dark = navigation.getParam("dark", false);

    // console.log('navigation.getParam("dark", false)', headerStyle)
    return {
      headerLeft: Back(navigation, null, dark),
      /* headerRight: LogoPageSubCate, */
      headerTintColor: Color.headerTintColor,
      headerStyle,
      headerTitle: Logo
      /* headerTitleStyle: { alignSelf: 'center' }, */
    };
  };

  componentWillMount() {
    this.props.navigation.setParams({});

    //console.log("getIdCategory", this.state.getcategory_id);

    OpencartWorker.productsByCategoryTag(
      this.state.getcategory_id,
      null,
      null,
      1
    )
      .then(items => {
        //console.log('productsByCategoryId', items)

        items.map(function (value, index) {
          //console.log("productsByCategory", value.name);

          //console.log('itemsitemsitemsitems', value)

        });

        this.setState({
          /*  ProductAll: items.category */
          ProductAll: items
        });

      })
      .catch(err => {
        //console.log("productsByCategoryId_err", err);
      });

    /* Image.getSize(
        value.image,
        (width, height) => {
          //console.log(`The image dimensions are ${width}x${height}`);

         
        },
        error => {
          console.error(`Couldn't get the image size: ${error.message}`);
        }
      ); */
  }

  /*   componentWillReceiveProps(nextProps) {
 
      console.log('nextProps', nextProps)
  } */

  render_item = item => {
    const { navigate } = this.props.navigation;
    console.log("subcate", item);

    return (
      <TouchableHighlight
        onPress={() =>
          navigate("SubProduct", {
            subcategorie_id: item.id,
            categories_id: this.state.getcategory_id,
            image_sub_cate: item.image,
            name: item.name
          })
        }
      >
        <View style={[styles.list_item_product]}>
          <Icon
            style={[styles.icon, styles.icon_style_more]}
            color={"black"}
            size={20}
            name={"chevron-small-right"}
          />

          <Text style={[styles.text_item]}>{item.name}</Text>
          <Text style={[styles.text_item]}>{item.more_name}</Text>
          

          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <Image
              source={{ uri: item.image }}
              style={[styles.image_product_cate]}
            />
          </View>
        </View>
      </TouchableHighlight>
    );
  };

  render() {
    const { navigate } = this.props.navigation;

    /*  const styles.width = Dimensions.get("window").width;
    const styles.height = Dimensions.get("window").height; */

    Image.getSize(this.state.getcategory_image.src, (width, height) => {
      if (styles.width && !styles.height) {
        this.setState({
          width: styles.width,
          height: height * (styles.width / width)
        });
      } else if (!styles.width && styles.height) {
        this.setState({
          width: width * (styles.height / height),
          height: styles.height
        });
      } else {
        this.setState({ width: width, height: height });
      }
    });
    console.log("size screen==", styles.height, " ", styles.width);
    console.log("width==", this.state.width, " ", this.state.height);

    if (
      this.state.getcategory_name !== "V-BELTS" &&
      this.state.getcategory_name !== "ACCU-LINK"
    ) {
      return (
        <View>
          <FlatList
            data={this.state.ProductAll}
            renderItem={({ item }) => this.render_item(item)}
            keyExtractor={item => item.id}
          />
        </View>
      );
    } else {
      const renderProductAll = [];

      if (this.state.ProductAll && this.state.getcategory_name === "ACCU-LINK") {
        // console.log("getcategory_sub_image", this.state.getcategory_sub_image);

        var sub_image2 = this.state.getcategory_sub_image;
        var category_name = this.state.getcategory_name;

        this.state.ProductAll.map(value => {
          console.log('productbycategory', value)

          renderProductAll.push(
            <View style={[styles.view_product]} >
              <TouchableHighlight
                onPress={() =>
                  navigate("SubProduct", {
                    subcategorie_id: value.id,
                    sub_image: sub_image2,
                    image_sub_cate: value.image,
                    name: value.name,
                    category_name: category_name
                  })
                }
              >
                <Image
                  source={{ uri: value.image }}
                  style={[styles.image_product]}
                />
              </TouchableHighlight>
              <View style={{ marginVertical: 20 }}>
                <Text>{value.name}</Text>
                <Text>{value.more_name}</Text>
              </View>
            </View >

          );
        });

      } else if (this.state.ProductAll && this.state.getcategory_name === "V-BELTS") {

        var sub_image2 = this.state.getcategory_sub_image;
        var category_name = this.state.getcategory_name;

        this.state.ProductAll.map(value => {
          renderProductAll.push(
            <TouchableHighlight
              onPress={() =>
                navigate("SubProduct", {
                  subcategorie_id: value.id,
                  sub_image: sub_image2,
                  image_sub_cate: value.image,
                  name: value.name,
                  category_name: category_name
                })
              }>
              <View
                style={{
                  borderWidth: 1,
                  borderColor: "black",
                  height: styles.height,
                  marginHorizontal: 10
                  /*  borderStyle: "dashed",
                  margin: 1 */
                }}
              >
                <Text>{value.name}</Text>
                <Text>{value.more_name}</Text>
                <View
                  style={{ alignItems: "center", justifyContent: "center" }}
                >
                  <Image
                    source={{ uri: value.image }}
                    style={[styles.product_cate]}
                  />
                </View>
              </View>
            </TouchableHighlight>
          );
        });


      }

      if (this.state.getcategory_name === "ACCU-LINK") {

        renderHeader = () => {

          return (<View><Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 20 }}>{'ACCU-LINK'}</Text></View>)

        }

      } else {


        renderHeader = () => {

          return (<View></View>)

        }


      }

      if (this.state.getcategory_name !== "ACCU-LINK") {
        renderImage = () => {
          return (
            <View style={([styles.viewbannerproductcate], { borderWidth: 0 })}>
              <Image
                source={{ uri: this.state.getcategory_image.src }}
                style={{
                  width: this.state.widht,
                  height: 100,
                  margin: 10
                }}
              />
            </View>
          );
        };
      } else {
        renderImage = () => {
          return (
            <View style={([styles.viewbannerproductcate], { borderWidth: 0 })}>
              <Image
                source={{ uri: this.state.getcategory_sub_image }}
                style={{
                  width: this.state.widht,
                  height: 100,
                  margin: 10
                }}
              />
            </View>
          );
        };
      }

      if (this.state.getcategory_name === "ACCU-LINK") {
        return (
          <ScrollView>
            {renderImage()}
            {renderHeader()}
            <View style={[styles.flatWrapOld]}>{renderProductAll}</View>
          </ScrollView>
        );

      } else if (this.state.getcategory_name === "V-BELTS") {

        return (
          <View>
            <FlatList
              data={this.state.ProductAll}
              renderItem={({ item }) => this.render_item(item)}
              keyExtractor={item => item.id}
            />
          </View>
        )


      }


    }
    /*  return (
      <View>
        <FlatList
          data={this.state.ProductAll}
          renderItem={({ item }) => this.render_item(item)}
          keyExtractor={item => item.id}
        />
      </View>
    ) */
  }
}
