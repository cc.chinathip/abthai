/** @format */

import React, { Component } from "react";
import { View, Text, Image, TouchableHighlight, FlatList, ScrollView, StyleSheet } from "react-native";
import { Card, ListItem, Button, Icon } from 'react-native-elements'
import { Color, Styles, withTheme, Images } from "@common";
import { News } from "@containers";
import { Menu, Logo, EmptyView, Back } from "./IconNav";
import * as OpencartWorker from '../services/OpencartWorker'
@withTheme
export default class NewsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );
    const dark = navigation.getParam("dark", false);

    return {
      headerLeft: Back(navigation, null, dark),
      headerRight: EmptyView(),

      headerTintColor: Color.headerTintColor,
      headerStyle,
    };
  };

  constructor(props) {
    super(props);
    const params = this.props.navigation.state.params;
    this.state = {

      NewsAll: null

    };
  }

  componentWillMount() {
    const {
      theme: {
        colors: { background },
        dark,
      },
    } = this.props;

    this.props.navigation.setParams({
      headerStyle: Styles.Common.toolbar(background, dark),
      dark,
    });

    OpencartWorker.getNotification()
      .then((items) => {
        //console.log('NewsAll', items)

        this.setState({
          NewsAll: items
        })

      })
      .catch((err) => {
        console.log('NewsAll err', err)

      })



  }

  componentWillReceiveProps(nextProps) {
    if (this.props.theme.dark !== nextProps.theme.dark) {
      const {
        theme: {
          colors: { background },
          dark,
        },
      } = nextProps;
      this.props.navigation.setParams({
        headerStyle: Styles.Common.toolbar(background, dark),
        dark,
      });
    }
  }


  render() {
    const styles = StyleSheet.create({
      image_news: {
        width: 100,
        height: 100,
        alignSelf: 'center',
        justifyContent: 'center',

      },

    });

    var dataDisplay = [];
    if (this.state.NewsAll) {
      this.state.NewsAll.map(function (value, index) {
        console.log('value', value.image)

        dataDisplay.push(<View key={index}>
          <Card title={value.content}>
            <Image
              style={[styles.image_news]}
              resizeMode="cover"
              source={{ uri: value.image }}
            />

          </Card>
        </View>);

      });
    }

    return (

      <ScrollView>
        <View style={{ flex: 1 }}>

          {dataDisplay}

        </View >
      </ScrollView>
    );

  }
}
