/** @format */

import React, { PureComponent } from "react";
import { Back, EmptyView } from "./IconNav";
import { View, Text, Image, TouchableHighlight, FlatList, ScrollView, StyleSheet } from "react-native";
import { Color, Styles, withTheme, Images, Config } from "@common";
import * as OpencartWorker from '../services/OpencartWorker'
//import styles from "../components/HorizonList/styles";
import Icon from "react-native-vector-icons/Entypo";
import PropTypes from "prop-types";
export default class PrivacyScreen extends PureComponent {

    constructor(props) {
        super(props);
        const params = this.props.navigation.state.params;
        this.state = {
            InformationAll: null,
            Information_id: params ? params.type_Information : null,
        };
        this.buttonList = [
            ...Config.menu.listMenu,

        ];
    }

    static propTypes = {
        type_Information: PropTypes.number,

    };

    static navigationOptions = ({ navigation }) => {
        console.log('navigation', navigation)


        const headerStyle = navigation.getParam(
            "headerStyle",
            Styles.Common.toolbar(),

        );
        const dark = navigation.getParam("dark", false);

        //const Information_id = navigation.getParam("type_Information", false);

        //console.log('Information_id', Information_id)


        // console.log('navigation.getParam("dark", false)', headerStyle)
        return {
            headerLeft: Back(navigation, null, dark),
            headerRight: EmptyView(),
            headerTintColor: Color.headerTintColor,
            headerStyle,
        };
    };


    componentWillMount() {

        OpencartWorker.getInformations(3)
            .then((items) => {
                //console.log('getInformations', items)

                this.setState({
                    InformationAll: items
                })

            })
            .catch((err) => {
                console.log('getInformations', err)

            })
    }


    render() {



        const styles = StyleSheet.create({
            title_privacy: {
                textAlign: 'center',
                fontWeight: 'bold',
                fontSize: 20
            }, description: {
                marginHorizontal: 10,
                marginTop: 5
            },
            container: {
                flex: 1,

            }

        });

        var dataDisplay = [];

        /* console.log('params', this.state.Information_id)
        this.buttonList.map(function (value_config, index_config) {

            console.log('value_config0', value_config.params)
        }); */


        if (this.state.InformationAll) {

            //console.log('InformationAll', this.state.InformationAll)

            this.state.InformationAll.map(function (value, index) {

                console.log('value', value.information_id)


                dataDisplay.push(<View key={index}>
                    <Text style={[styles.title_privacy]}>{value.title}</Text>
                    <Text style={[styles.description]}>{value.description}</Text>
                </View>);


            });

        } else {

            console.log('InformationAll Error')
        }






        return (
            <ScrollView>
                <View style={[styles.container]}>
                    {dataDisplay}
                </View>
            </ScrollView>
        );


    }
}
