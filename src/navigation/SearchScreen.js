/** @format */

import React, { PureComponent } from "react";
import { Search } from "@components";
import { View, Text, Image, TouchableHighlight, FlatList, ScrollView, StyleSheet, Button } from "react-native";
import { SearchBar } from 'react-native-elements';
import { Languages } from "@app/Omni";
import * as OpencartWorker from '../services/OpencartWorker'
export default class SearchScreen extends PureComponent {

  constructor(props) {
    super(props);
    const params = this.props.navigation.state.params;
    this.state = {
      search: '',
      ProductAll: null
    };
  }

  static navigationOptions = ({ navigation }) => ({
    title: "Search",
    header: null,
    // tabBarVisible: false,
    tabBarLabel: null,

  });



  componentWillMount() {

    OpencartWorker.productsByName(this.state.search, 5, 1)
      .then((items) => {
        //console.log('productsByName', items)

        this.setState({
          ProductAll: items,
          status: true
        })

      })
      .catch((err) => {
        console.log('productsByName err', err)

      })
  }



  componentWillUpdate(nextProps, nextState) {



    if (nextState.search === this.state.search) {

      //console.log('nextState', nextState.search)

      this.state.search = false;

      OpencartWorker.productsByName(nextState.search, 5, 1)
        .then((items) => {
          //console.log('productsByName', items)

          this.setState({
            ProductAll: items,
            //search: true
          })

        })
        .catch((err) => {
          console.log('productsByName err', err)

        })



    } else {

      console.log('stop render')
    }


  }


  updateSearch = search => {
    this.setState({
      search,

    });

    console.log('update search', search)
  };

  render() {
    //this.DisplayProduct()
    //console.log('this.state.search under render', this.state.search)


    var renderProductAll = [];
    const styles = StyleSheet.create({

      container: {
        flex: 1,
        marginVertical: 10

      }, flatWrap: {
        flex: 1,
        paddingLeft: 0,
        marginTop: 50,
        /*  marginBottom: 15, */
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',

      }, container_category: {

        alignItems: 'center',
        width: '50%',
        borderWidth: 0.5,
        borderColor: '#d6d7da',

      },

    });
    const { navigate } = this.props.navigation;
    if (this.state.ProductAll) {
      this.state.ProductAll.map(function (value, index) {

        if (value.image !== '') {
          //console.log('image', value.product_id)

          renderProductAll.push(
            <View style={[styles.container_category]} key={index} >
              <TouchableHighlight
                onPress={() => navigate('DetailScreen', { product: value })}>
                <Image style={{ width: 150, height: 150 }} source={{ uri: value.image }} />
              </TouchableHighlight>
              <View style={{ marginVertical: 20 }}><Text>{value.name}</Text></View>
            </View>);
        }

      });
    } else {

      console.log('productsByName not found')
    }

    return (
      <ScrollView style={[styles.container]}>
        <SearchBar
          placeholder={'Search..'}
          onChangeText={this.updateSearch}
          value={this.state.search}
          searchIcon={{ size: 24 }}
        />

        <View style={[styles.flatWrap,]}>{renderProductAll}</View>
      </ScrollView>

    );
  }
}
