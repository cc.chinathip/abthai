/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  ScrollView,
  TouchableOpacity,
} from "react-native";

import { warn } from "@app/Omni";
import { Color, Styles, withTheme } from "@common";
import { Home } from "@containers";
import { Menu, NavBarLogo, HeaderHomeRight, Logo, Logo2 } from "./IconNav";
import * as OpencartWorker from "../services/OpencartWorker";
import styles from "../components/HorizonList/styles";
import Swiper from "react-native-swiper";

@withTheme
export default class HomeScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );
    const dark = navigation.getParam("dark", false);

    //console.log('Home Screen')
    return {
      //headerTitle: NavBarLogo({ navigation }),
      headerLeft: Menu(dark),
      /*  headerRight: HeaderHomeRight(navigation), */

      headerTintColor: Color.headerTintColor,
      headerStyle,
      headerTitleStyle: Styles.Common.headerStyle,

      // use to fix the border bottom
      headerTransparent: true,
      headerTitle: Logo,
    };
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      CategoriesAll: null,
      BannerHome: null,
    };
  }

  async componentWillMount() {
    const {
      theme: {
        colors: { background },
        dark,
      },
    } = this.props;

    this.props.navigation.setParams({
      headerStyle: Styles.Common.toolbar(background, dark),
      dark,
    });

    await OpencartWorker.getCategories()
      .then((items) => {
        //console.log('getCategories', items)

        this.setState({
          CategoriesAll: items,
        });
      })
      .catch((err) => {
        //console.log('OpencartWorker.getCategories()', err)
      });

    await OpencartWorker.getBanner()
      .then((items) => {
        //console.log('getBanner', items)

        this.setState({
          BannerHome: items,
        });
      })
      .catch((err) => {
        console.log("getBanner", err);
      });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.theme.dark !== nextProps.theme.dark) {
      const {
        theme: {
          colors: { background },
          dark,
        },
      } = nextProps;
      this.props.navigation.setParams({
        headerStyle: Styles.Common.toolbar(background, dark),
        dark,
      });
    }

    // console.log('nextProps', this.state.test)
  }

  render() {
    var renderCategoriesAll = [];
    const { navigate } = this.props.navigation;

    //console.log('BannerHome', this.state.BannerHome)

    if (this.state.CategoriesAll) {
      this.state.CategoriesAll.map(function(value, index) {
        if (value.image.src !== "") {
          console.log("category data", value);

          renderCategoriesAll.push(
            <View style={[styles.container_category]} key={index}>
              <TouchableHighlight
                onPress={() =>
                  navigate("ProductByCategory", {
                    category_id: value.id,
                    category_name: value.name,
                    category_image: value.image,
                    category_sub_image: value.sub_image,
                  })
                }
              >
                <Image
                  /*  style={{ width: 150, height: 150 }} */
                  style={[styles.image_product_cate]}
                  source={{ uri: value.image.src }}
                />
              </TouchableHighlight>
              <View style={{ marginVertical: 20 }}>
                <Text>{value.name}</Text>
              </View>
            </View>
          );
        }
      });
    } else {
      console.log("CategoriesAll not found");
    }

    var renderBannerAll = [];
    //console.log('value.image', this.state.BannerHome)
    if (this.state.BannerHome) {
      renderBannerAll.push(
        <View
          style={{
            marginTop: 50,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {this.state.BannerHome.length == 0 ? (
            <View style={{ height: 200 }}> </View>
          ) : (
            <Swiper
              height={300}
              loop={false}
              showsPagination={false}
              removeClippedSubviews={false}
              containerStyle={{ alignSelf: "stretch" }}
            >
              {this.state.BannerHome.map((value, index) => (
                <Image
                  source={{ uri: value.image }}
                  style={[styles.BannerHome]}
                />
              ))}
            </Swiper>
          )}
        </View>
      );
    } else {
      console.log("renderBannerAll not found-");
    }

    return (
      <ScrollView>
        <View>{renderBannerAll}</View>
        <View style={[styles.flatWrap]}>{renderCategoriesAll}</View>
      </ScrollView>
    );
  }
}
