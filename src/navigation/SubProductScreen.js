/** @format */

import React, { PureComponent } from "react";
import { Back, EmptyView, Logo } from "./IconNav";
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  FlatList,
  ScrollView
} from "react-native";
import { Color, Styles, withTheme, Images } from "@common";
import * as OpencartWorker from "../services/OpencartWorker";
import styles from "../components/HorizonList/styles";
import Icon from "react-native-vector-icons/Entypo";
export default class SubProductScreen extends PureComponent {
  constructor(props) {
    super(props);
    const params = this.props.navigation.state.params;
    this.state = {
      get_subcategorie_id: params ? params.subcategorie_id : null,
      get_categories_id: params ? params.categories_id : null,
      ProductAll: null,
      get_image_sub_cate: params ? params.image_sub_cate : null,
      get_name: params ? params.name : null,
      get_sub_image: params ? params.sub_image : null,
      get_category_name: params ? params.category_name : null
    };
  }

  static navigationOptions = ({ navigation }) => {
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );
    const dark = navigation.getParam("dark", false);

    // console.log('navigation.getParam("dark", false)', headerStyle)
    //console.log('SubProductScreen navigation', navigation)
    return {
      headerLeft: Back(navigation, null, dark),
      headerRight: EmptyView(),

      headerTintColor: Color.headerTintColor,
      headerStyle,
      headerTitle: Logo
    };
  };

  componentWillMount() {

    /*  console.log("subPoduct",
       this.state.get_subcategorie_id,
       "categories_id ",
       this.state.get_categories_id,
       'image', ' ', this.state.get_image_sub_cate
     ); */
    //console.log('subPoduct', this.state.get_sub_image, ' ', this.state.get_category_name)

    OpencartWorker.productsBySubategory(this.state.get_subcategorie_id, null, 1)
      .then(items => {
        items.map(function (value, index) {
          //console.log("productsBySubategory", value.name);
        });

        /* console.log('itemsitemsitemsitems', items) */

        this.setState({
          ProductAll: items
        });

      })
      .catch(err => {
        // console.log('productsBySubategory', err)
      });
  }

  render() {
    var renderProductAll = [];
    const { navigate } = this.props.navigation;

    // console.log('navigation', this.state.CategoriesAll)

    var category_name = this.state.get_category_name;
    var get_sub_image = this.state.get_sub_image;

    if (category_name !== "ACCU-LINK") {
      console.log("check not ACCU-LINK");

      renderImageBanner = () => {
        return (
          <Image
            style={[styles.image_banner]}
            source={{ uri: this.state.get_image_sub_cate }}
          />
        );
      };
    } else {
      console.log("check ACCU-LINK");

      renderImageBanner = () => {
        return (
          <Image
            style={[styles.image_banner]}
            source={{ uri: get_sub_image }}
          />
        );
      };
    }

    if (this.state.ProductAll) {
      console.log('productsBySubategory')
      this.state.ProductAll.map(function (value, index) {
        if (value.image !== "") {
          /* console.log(
            "this.state.get_sub_image",
            get_sub_image,
            " ",
            category_name
          ); */

          console.log('Subproduct', value)


          renderProductAll.push(
            <View style={[styles.view_product]} key={index}>
              <TouchableHighlight

                onPress={() =>
                  navigate("DetailScreen", {
                    product: value
                    /* categories_id: this.state.get_categories_id */
                  })
                }

              >
                <Image
                  style={[styles.image_product]}
                  source={{ uri: value.image }}
                />
              </TouchableHighlight>
              <View style={{ marginVertical: 20 }}>
                <Text>{value.name}</Text>
              </View>
            </View>
          );

        }
      });
    } else {
      //console.log('ProductAll not found')
    }

    return (
      <ScrollView>
        <View style={[styles.view_banner]}>{renderImageBanner()}</View>
        <View style={[styles.view_name_banner]}>
          <Text style={[styles.text_name_banner]}>{this.state.get_name}</Text>
        </View>
        <View style={[styles.flatWrapOld]}>{renderProductAll}</View>
      </ScrollView>
    );
  }
}
