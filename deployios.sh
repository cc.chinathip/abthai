#! /usr/bin/env bash
set -e # exit entire script when command exits with non-zero status

expo publish
expo build:ios --release-channel production --non-interactive
curl -o app.ipa "$(expo url:ipa --non-interactive)"

#### 7. Submit standalone iOS app to iTunes Connect ####
# Make sure the following env variables are set
# export DELIVER_USERNAME=<your-itunes-connect-email>
# export DELIVER_PASSWORD=<your-itunes-connect-password>

# Use fastlane to upload your current standalone iOS build to itc
# fastlane deliver --verbose --ipa "app.ipa" --skip_screenshots --skip_metadata 
FASTLANE_ITC_TEAM_ID="1467137484" bundle exec fastlane pilot upload ipa --distribute_external --groups "Tester" --username "varodom.naul@gmail.com" --demo_account_required --beta_app_feedback_email "varodom.naul@gmail.com" --beta_app_description "This is a description of my app" --notify_external_testers --changelog "This is my changelog of things that have changed in a log"
#### Misc ####
# [Optional] You may or may not need to do this depending on your setup.
# expo logout
