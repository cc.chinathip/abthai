#! /usr/bin/env bash

#### 2. Script Setup ####
# It's useful to exit the bash script when a command exits with a non-zero status
# as the following commands must be run successfully in sequence for expected results.
set -e # exit entire script when command exits with non-zero status
expo publish
#### 4. Building Android Standalone App ####
# Start building standalone android build using `production` release channel
expo build:android --release-channel production --non-interactive
# Download the artifact to current directory as `app.apk`
curl -o app.apk "$(expo url:apk --non-interactive)"
# fastlane supply --track 'production' --json_key '<path/to/json_key.json>' --package_name "<your-package-name>" --apk "app.apk" --skip_upload_metadata --skip_upload_images --skip_upload_screenshots
firebase appdistribution:distribute app.apk --app 1:765327032160:android:41a0ddf84c1b39b9382805 --groups "tester-external, tester" --release-notes "Beta"
