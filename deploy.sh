#! /usr/bin/env bash
set -e # exit entire script when command exits with non-zero status
expo publish
expo build:ios --release-channel production --non-interactive
expo build:android --release-channel production --non-interactive
curl -o app.apk "$(expo url:apk --non-interactive)"
curl -o app.ipa "$(expo url:ipa --non-interactive)"
FASTLANE_ITC_TEAM_ID="119562380" bundle exec fastlane pilot upload ipa
